function sol = odeEuler(ode,tspan,y0,h,extargs)

t = tspan(1):h:tspan(2);

y = zeros(length(y0),length(t));

y(:,1) = y0;

for i = 1:length(t)-1
    dy_dt = ode(t(i),y(:,i),extargs);
    y(:,i+1) = y(:,i) + dy_dt*h;
end

sol = struct();
sol.t = t;
sol.y = y;
end  
