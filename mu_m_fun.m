function mu_m = mu_m_fun(pstruct,...
    n_d,...
    Xtal)
%calculates melt viscosity                        
%Josh Crozier 2019 
%(modified from code provided by Michael Manga)


%melt/magma viscosity parameters
% Costa 2006 + Hess and Dingwell 1996
c1 = 0.9995; %fitting coefficient
c2 = 0.4; %fitting coefficient
c3 = 1; %fitting coefficient
einsteinB = 2.5; %coefficient for crystal effect

if isreal((pi^0.5/2)*Xtal*(1+(c2/(1-Xtal)^c3)) )
    thetaX = (1 - c1* erf((pi^0.5/2)*Xtal*(1+(c2/(1-Xtal)^c3))))^(-einsteinB/c1);
else
    'WARNING: viscosity trouble!'
    display(Xtal)
    display(P)
    thetaX =1;
end


%original version in code from Micahel Manga
muMelt = 10^(-3.62517 + 0.248398*log(100*n_d) + (9601-2368*log(100*n_d))...
   /(pstruct.Temp - (195.7 + 96.4931*log(100*n_d))));

%version listed in Degruyter 2012 cited from Kozono and Koyaguchi 2009
% muMelt = 10^(-3.545 + 0.833*log(100*n_d) + (9601-2368*log(100*n_d))...
%    /(pstruct.Temp - (195.7 + 32.25*log(100*n_d))));

mu_m = muMelt*thetaX;

end