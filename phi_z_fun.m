function [phi_z_target,dphi_dz_target] = phi_z_fun(z,pstruct)
%function for prescribing porosity threshold as function of depth

% phi_shape = 'linear';
% phi_shape = 'quartersinusoid';
phi_shape = 'halfsinusoid';

phi_maxthresh = pstruct.phi_maxthresh; %upper threshold
phi_final = pstruct.phi_final; %final value
phi_transitiondepth = pstruct.phi_transitiondepth; %depth at which porosity starts tapering down linearly


if z < - phi_transitiondepth
    phi_z_target = phi_maxthresh;
    dphi_dz_target = 0;
elseif z <= 0
    if strcmp(phi_shape,'linear')
        %linear
        dphi_dz_target = (phi_final-phi_maxthresh)/(phi_transitiondepth);
        phi_z_target = phi_final + dphi_dz_target*z;
    elseif strcmp(phi_shape,'halfsinusoid')
        amp = (phi_maxthresh-phi_final)/2;
        center = (phi_final+phi_maxthresh)/2;
        period = phi_transitiondepth*2;
        phi_z_target = center - amp*cos(2*pi*z/period);
        dphi_dz_target = 2*pi/period*amp*sin(2*pi*z/period);
    else
    end
else
    phi_z_target = phi_final;
    dphi_dz_target = 0;
end
end

