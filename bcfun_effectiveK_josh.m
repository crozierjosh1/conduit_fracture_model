function [bc0, bcend, bc0_mask, bcend_mask] = bcfun_effectiveK_josh(var_vec_0, var_vec_end, pstruct)
%%boundary conditions for homeade bvp solver
%josh crozier 2019

Patm = pstruct.Patm;
gasConst = pstruct.gasConst;
Temp = pstruct.Temp;
rho_m = pstruct.rho_m;
n_v0 = pstruct.n_v0;

P0 = var_vec_0(1);
% phi0 = var_vec_0(2);
% Pend = var_vec_end(1);
% phiend = var_vec_end(2);

%porosity in no-flux case
rho_g0 = P0/(gasConst*Temp);
[n_d0, ~] = solubility_fun(pstruct, P0);
n_g0 = (n_v0 - n_d0)/(1 - n_d0);
if n_g0 < 0
    n_g0 = 0;
end
%assuming no fractures initially, and no gas flux
%!!!not properly accounting for crystals!!!!
target_phi0 = (n_g0/rho_g0)...
    /(n_g0/rho_g0 + (1 - n_g0)/rho_m);

bc0 = [NaN,target_phi0];
bc0_mask = [true,false];

bcend = [Patm,NaN];
bcend_mask = [false,true];

end

