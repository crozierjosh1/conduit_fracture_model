function [pstruct,init_var_vec] = shoot_IC_fun_minphi(pstruct, shoot,...
    shootvar_val)
% initial conditions for shooting method
% Josh Crozier 2020

%% values adjusted during shooting
if strcmp(shoot.shootvar, 'P0')
    pstruct.P0 = shootvar_val;
elseif strcmp(shoot.shootvar, 'q')
    pstruct.q = shootvar_var;
elseif strcmp(shoot.shootvar, 'r_base')
    pstruct.r_base = shootvar_val;
else
    error('invalid shootval')
end

%% other values
pstruct.rho_g0 = pstruct.P0/(pstruct.gasConst*pstruct.Temp);
[n_d0, ~] = solubility_fun(pstruct, pstruct.P0);
pstruct.n_d0 = n_d0;
pstruct.n_g0 = (pstruct.n_v0 - pstruct.n_d0)/(1-pstruct.n_d0);
if pstruct.n_g0 < 0
    pstruct.n_g0 = 0;
end
%porosity (in bubbles)
%assuming no fractures initially, and no gas flux
%!!!not properly accounting for crystals!!!!
pstruct.phi0 = (pstruct.n_g0/pstruct.rho_g0)...
    /(pstruct.n_g0/pstruct.rho_g0 + (1-pstruct.n_g0)/pstruct.rho_m);

[r_c0, A0, ~] = A_fun(pstruct, -pstruct.L);
pstruct.r_c0 = r_c0;
pstruct.A0 = A0;

pstruct.q_l = pstruct.q*(1 - pstruct.n_v0); %non-volatile flux
pstruct.q_v = pstruct.q*pstruct.n_v0; %volatile flux (exsolved+dissolved)

%% initial conditions vector
if pstruct.use_k_w
    init_var_vec = [pstruct.P0;pstruct.phi0;0];
else
    init_var_vec = [pstruct.P0;pstruct.phi0];
end

end

