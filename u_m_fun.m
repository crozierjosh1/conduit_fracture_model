function u_m = u_m_fun(pstruct,...
    A,...
    A_f,...
    phi,...
    n_d)
%calculates melt velocity from conseration of mass
%Josh Crozier 2019

q_l = pstruct.q_l;
rho_m = pstruct.rho_m;

%conservation of liquid (non-volatile)
u_m = q_l/((A - A_f)*(1 - phi)*(1 - n_d)*rho_m);

end