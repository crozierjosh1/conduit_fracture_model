%plot conduit geom

pstruct = struct('flare_shape','sin',...
    'r_base',2.5,...
    'r_flare',5,...
    'flare_depth',100,...
    'L',3000);

zplot = linspace(-pstruct.flare_depth,500,5000);
Rplot = NaN(size(zplot));

for jj = 1:length(zplot)
    Rplot(jj) = A_fun(pstruct,zplot(jj));
end

figure()
plot(zplot,Rplot)