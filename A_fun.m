function [r_c, A, dA_dz] = A_fun(pstruct, z)
%calculates conduit radius and derivative
%Josh Crozier 2020

min_r_abovesurf = 1; %min radius above surface
%in case taper produces very small/negative radius [m]
r_abovesurf_frac = 0.1;

flare_shape = pstruct.flare_shape; %'quartersin' or 'halfsin' or 'exp'
r_base = pstruct.r_base;
r_flare = pstruct.r_flare; % radius at top of conduit [m]
flare_depth = pstruct.flare_depth; % depth of flare decay [m]
L = pstruct.L;

r_diff = r_flare - r_base;
r_abovesurf = r_flare + r_base*r_abovesurf_frac;
r_diff_abovesurf = r_abovesurf - r_flare;
flare_depth_abovesurf = r_diff_abovesurf/r_diff*flare_depth;

if strcmp(flare_shape,'exp')
    %exponential taper
    decay = -log(0.5)/flare_depth;
    if z <= 0
        r_c = r_base + r_diff*exp(z*decay);       
        dr_c_dz = r_diff*decay*exp(z*decay);
        
    else
        %try linear after ground surface (seems to help shooting)
        %dr_c_dz = r_flare/L;
        %keep gradient of exp at z=0
        dr_c_dz = r_diff*decay;
        r_c = r_base + r_diff + dr_c_dz*z;
    end
    
elseif strcmp(flare_shape,'quartersin')
    %sinusoidal taper
    if z <= -flare_depth
        r_c = r_base;
        dr_c_dz = 0;
    elseif z > -flare_depth && z <= 0
        amp = (r_flare-r_base);
        center = r_flare;
        period = flare_depth*4;
        r_c = center + amp*sin(2*pi*z/period);
        dr_c_dz = 2*pi/period*amp*cos(2*pi*z/period);
    elseif z > 0 && z < flare_depth_abovesurf
        r_c = r_flare +...
            r_diff_abovesurf*sin(pi/(2*flare_depth_abovesurf)*z);
        dr_c_dz = r_diff_abovesurf*pi/(2*flare_depth_abovesurf)*...
            cos(pi/(2*flare_depth_abovesurf)*z);
    else
        r_c = r_abovesurf;
        dr_c_dz = 0;
    end
    
elseif strcmp(flare_shape,'halfsin')
    %sinusoidal taper
    if z <= -flare_depth
        r_c = r_base;
        dr_c_dz = 0;
    elseif z > -flare_depth && z <= 0
        amp = (r_flare-r_base)/2;
        center = (r_flare+r_base)/2;
        period = flare_depth*2;
        r_c = center + amp*cos(2*pi*z/period);
        dr_c_dz = -2*pi/period*amp*sin(2*pi*z/period);
    elseif z > 0 
        r_c = r_flare;
        dr_c_dz = 0;
    end
    
else
    error('invalid flare shape')
end

%in case region above surface has negative radius due to taper:
if z > 0 && r_c <= min_r_abovesurf && ~strcmp(flare_shape,'halfsin')
    r_c = min_r_abovesurf;
    dr_c_dz = 0;
end

A = pi*r_c^2;
dA_dz = 2*pi*r_c*dr_c_dz;

end
