%%main script for conduit model using simple effective permeability method
%josh crozier 2019
%based on Degruyter 2012 model and code obtained via Michael Manga

%all parameters than should generally be tinkered with are in the sections 
%of this script titled:
    %"GENERAL PARAMETERS",
    %"PLOT, OUTPUT, SEARCH PARAMETERS", 
    %"PARFOR", and
    %"TECHNICAL PARAMETERS"
%the code is set up to solve the boundary value problem using either a
%shooting method or relaxation method
%multiple parameters can be chosen to grid-search over, this can be done in
%a parfor loop (though plots will not display in parfor loop)

clearvars

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GENERAL PARAMETERS
main_pstruct = struct(); %structure for storing parameters

%% general constants
main_pstruct.g = 9.81; % gravitational acceleration [m s^-2]
main_pstruct.gasConst = 462; % specific gas constant for water [J kg^-1 K^-1]
main_pstruct.satConst = 4.11e-6; % saturation constant (Henry's law) [Pa^-0.5] 4.11e-6
main_pstruct.mu_g = 1e-5; % gas viscositiy [Pa s]
main_pstruct.Patm = 1e5; % atmospheric pressure [Pa]
% main_pstruct.rho_c = 2500; % crust density

%% magma properties
main_pstruct.Temp = 900 + 273; % temperature [K]
main_pstruct.rho_m = 2400; % melt density = crystal density [kg/m^-3]
main_pstruct.Xtal = 0.00; % initial Xtal mass fraction with respect to the groundmass (Xtals+melt) [ ]
main_pstruct.n_v0 = 0.03; %0.04/(1-Xtal); % initial total water mass fraction
%recommend always have some small amount of exsolved gas, represent CO2 and
%also seems to help numerics (otherwise might need to build in more
%conditional operations)
main_pstruct.n_g_permanent = 0.001; %minimum amount of exsolved gas

%% conduit geometry
main_pstruct.L = 3000; % conduit length [m]
% main_pstruct.r_base = 25; % conduit radius [m]
main_pstruct.r_flare = 30; % radius at top of conduit [m]
main_pstruct.flare_depth = 150; % depth of flare start (sin) or half-depth of flare decay (exp) [m]
main_pstruct.flare_shape = 'quartersin'; %'quartersin' or 'halfsin' 'exp'
%if flare is too steep the solver will have trouble converging

%% conduit wall gas loss
main_pstruct.use_k_w = true; %false to not use conduit wall permeability
main_pstruct.rho_w = 2800; %wall rock density (for lithostatic pressure)
main_pstruct.k_w = 1E-13; %wall rock darcy permeability
main_pstruct.L_w = 100; %wall rock pressure diffusion length scale

%% pressure and mass flux
main_pstruct.q = 1E4;
main_pstruct.P0 = main_pstruct.L*9.8*2800;

%% fracture properties
% main_pstruct.u_gf = 10; %gas in fracture velocity
main_pstruct.N_f = 10; %fracture count
%higher number decreases u_gf slightly
main_pstruct.highRe_Fanningfrictionfactor = 0.03;%0.03; %friction correction for vent wall roughness
%higher number helps keep u_gf down

%% flow regime thresholds (gas volume fraction)
% recommend setting both negative, otherwise numerical issues can pop up at
% transition (even though transition is tapered)
main_pstruct.phi_pL = -0.2; % start transition to percolation
main_pstruct.phi_pU = -0.1; % full percolation

%% prescribed porosity thresholds
main_pstruct.phi_maxthresh = 0.85; %upper threshold
%needs to be above phi_crit (0.74) or won't have any bubble network flow
%should be beneath reasonable fragmentation threshold (around 0.9)
%setting slightly lower will ensure solutions approach threshold steeply,
%in which case the damping will work better
main_pstruct.phi_final = 0.25; %final value
%observations at cordon caulle range from 0-0.45, mostly < 0.2
main_pstruct.phi_transitiondepth = 150; %depth at which porosity starts tapering down linearly
%suggest placing near or above vent flare where outgassing will naturally
%occur easier and bubble phi_crit will have been exceeded
main_pstruct.phi_damping_thresh = 0.005; %porosity difference from thresh at which to start tapering
main_pstruct.phi_damping_dist = 5; %distance over which to target intersection with thresh
% distance shouldn't be too large or else will detect region where phi_thresh
% decreases rather than flat region, so a few meters or less
% ratio phi_damping_thresh/phi_damping_dist will determine initial slope
% ratio should be smaller than all actual dphi/dz values or will induce
% artificially fast increase in phi
% dphi/dz values of interest seem to be as low as 0.001
% setting this ratio too low wil result in overly abrupt transition
% as currently implemented, impossible to get completely smooth transition
% in general, so will need to be tuned some for each solution, but in
% practice doesn't seem to matter too much

% main_pstruct.frac_damping_scale = 0.5; %scale for how quickly to introduce fractures (0-1)

%% bubble parameters
%!!! none of these should actually be used in this model
main_pstruct.N_b = 10^(14); % bubble number density in m^-3
main_pstruct.tortuosityfactor = 1; %tortuosity factor
main_pstruct.bubbleroughnessfactor = 10; % friction/bubble roughness factor
main_pstruct.throatratio = 0.3; % throat/bubble size ratio
main_pstruct.r_b_min = 1E-6; %minimum bubble radius allowed
%gonnermann 2017 glass mountaint values
main_pstruct.perm_b = 10^(-8.892);
main_pstruct.perm_n = 2;
main_pstruct.perm_phi_crit = 0.74;
%(keeps drag force from being really large which helps numerics)

%% solver-dependent parameters
use_bvp = false; %use relaxation solver instead of shooting
%relaxation solvers don't seem to work currently
shoot = struct();%initialize empty structure for shooting parameters
if use_bvp
    %shooting method currently only adjusts r_base
    shoot.shootvar = 'r_base';
    main_pstruct.q = 10*main_pstruct.rho_m; %total mass flux
    main_pstruct.P0 = 2800*main_pstruct.L*9.8;
else
    %specify either P0 or q or r_base, the others will be obtained by shooting method
    shoot.shootvar = 'r_base';
    % shoot_val = 'q';
    if strcmp(shoot.shootvar, 'q')
        main_pstruct.P0 = main_pstruct.L*main_pstruct.g*main_pstruct.rho_m + main_pstruct.Patm; % initial pressure
        main_pstruct.r_base = 25;
    elseif strcmp(shoot.shootvar, 'P0')
        main_pstruct.q = 10*main_pstruct.rho_m; %total mass flux
        main_pstruct.r_base = 25;
    elseif strcmp(shoot.shootvar, 'r_base')
%         main_pstruct.P0 = 2800*main_pstruct.L*9.8;
        %main_pstruct.P0 = -1E6 + main_pstruct.L*main_pstruct.g*main_pstruct.rho_m + main_pstruct.Patm; % initial pressure
        %main_pstruct.q = 10*main_pstruct.rho_m; %total mass flux
    else
        error('invalid shoot_val')
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PLOT, OUTPUT, & SEARCH PARAMETERS
% true to use settings default for cluster (supercomputer or just many runs)
cluster_settings = false; 
sgrid = struct();
sgrid.var_name = {};
sgrid.var_val = {};
sgrid.var_text = {};
sgrid.var_default = {};
if ~cluster_settings
    %% non-cluster
    %variable to grid search over
    %!!!currently might only work for 2 values!!!
%     sgrid.var_name{end+1} = 'r_flare';
%     sgrid.var_val{end+1} = [20];
%     sgrid.var_name{end+1} = 'flare_depth';
%     sgrid.var_val{end+1} = [100];
%     sgrid.var_name{end+1} = 'n_v0';
%     sgrid.var_val{end+1} = [0.01];
%     sgrid.var_name{end+1} = 'tortuosityfactor';
%     sgrid.var_val{end+1} = [1];
%     sgrid.var_name{end+1} = 'phi_f';
%     sgrid.var_val{end+1} = [0.8];
%     sgrid.var_name{end+1} = 'highRe_Fanningfrictionfactor';
%     sgrid.var_val{end+1} = [0.03];

%     sgrid.var_name{end+1} = 'P0'; %lower value decreases u_gf slightly
%     sgrid.var_text{end+1} = 'base pressure (Pa)';
%     sgrid.var_val{end+1} = main_pstruct.L*9.8*[2800];
%     sgrid.var_default{end+1} = main_pstruct.L*9.8*2800;
%     
%     sgrid.var_name{end+1} = 'q';  %lower value decreases u_gf slightly
%     sgrid.var_text{end+1} = 'mass flux (kg/s)';
%     sgrid.var_val{end+1} = [1E4];
%     sgrid.var_default{end+1} = 1E4;
%     
%     sgrid.var_name{end+1} = 'k_w';
%     sgrid.var_text{end+1} = 'wall permeability\newline(m^2)';
%     sgrid.var_val{end+1} = [1E-13];%[1E-14,1E-13,1E-12,1E-11];
%     sgrid.var_default{end+1} = 1E-13;

    sgrid.var_name{end+1} = 'flare_depth';
    sgrid.var_text{end+1} = 'flare\newlinedepth (m)';
    sgrid.var_val{end+1} = [50,250];
    sgrid.var_default{end+1} = 150;
    
    sgrid.var_name{end+1} = 'phi_transitiondepth';
    sgrid.var_text{end+1} = 'compaction\newlinedepth (m)';
    sgrid.var_val{end+1} = [50,250];
    sgrid.var_default{end+1} = 150;
    
    %plotting and output
    save_grid = false;
    save_sols = false;
    plot_depth_profiles = true;
    post_calc = true; %needed for plots
    if ~use_bvp
        shoot.shoot_plot = true; %plot shooting method progress
        shoot.plot_ode = false; %plot ode progress (very slow)
    else
        bvp_plot = false; %currently only does anything with homeade bvp solver
    end
    slices = true; %slices rather than full grid
else
    %% cluster
    %variable to grid search over
    %!!!currently might only work for 2 values!!!
    sgrid.var_name{end+1} = 'P0';
    sgrid.var_text{end+1} = 'base pressure\newline(Pa)';
    sgrid.var_val{end+1} = main_pstruct.L*9.8*(2400:200:3200);%[2600,2800,3000];
    sgrid.var_default{end+1} = main_pstruct.L*9.8*2800;
    
    sgrid.var_name{end+1} = 'q';
    sgrid.var_text{end+1} = 'mass flux\newline(kg/s)';
    sgrid.var_val{end+1} = logspace(2,6,9);
    sgrid.var_default{end+1} = 1E4;
    
    sgrid.var_name{end+1} = 'n_v0';
    sgrid.var_text{end+1} = 'initial H2O\newline(wt%)';
    sgrid.var_val{end+1} = (0.01:0.01:0.05);%[0.02,0.03,0.04];
    sgrid.var_default{end+1} = 0.03;
    
    sgrid.var_name{end+1} = 'r_flare';
    sgrid.var_text{end+1} = 'flare radius\newline(m)';
    sgrid.var_val{end+1} = (10:5:40);%[10,25,40];
    sgrid.var_default{end+1} = 30;
    
    sgrid.var_name{end+1} = 'flare_depth';
    sgrid.var_text{end+1} = 'flare\newlinedepth (m)';
    sgrid.var_val{end+1} = (50:50:300);%[50,125,200];
    sgrid.var_default{end+1} = 150;
    
    sgrid.var_name{end+1} = 'phi_transitiondepth';
    sgrid.var_text{end+1} = 'compaction\newlinedepth (m)';
    sgrid.var_val{end+1} = (50:50:300);%[50,125,200];
    sgrid.var_default{end+1} = 150;

    sgrid.var_name{end+1} = 'phi_final';
    sgrid.var_text{end+1} = 'vent porosity';
    sgrid.var_val{end+1} = 0.1:0.15:0.85;%[0.1,0.25,0.4];
    sgrid.var_default{end+1} = 0.25;
    
    sgrid.var_name{end+1} = 'N_f';
    sgrid.var_text{end+1} = 'fracture count';
    sgrid.var_val{end+1} = [4,10,40,100,400,1000];%[10,100,1000];
    sgrid.var_default{end+1} = 10;
    
    sgrid.var_name{end+1} = 'k_w';
    sgrid.var_text{end+1} = 'wall permeability\newline(m^2)';
    sgrid.var_val{end+1} = logspace(-16,-12,5);
    sgrid.var_default{end+1} = 1E-15;
    
%     sgrid.var_name{end+1} = 'tortuosityfactor';
%     sgrid.var_val{end+1} = [1,5,10];%linspace(4,14,4);
%     sgrid.var_name{end+1} = 'phi_f';
%     sgrid.var_val{end+1} = [0.40,0.50,0.60,0.70];
%     sgrid.var_name{end+1} = 'highRe_Fanningfrictionfactor';
%     sgrid.var_val{end+1} = [0.01,0.03,0.07];%linspace(4,14,4);
    
    %plotting and output
    save_grid = true;
    save_sols = false;
    plot_depth_profiles = false;
    post_calc = true; %needed for plots
    if ~use_bvp
        shoot.shoot_plot = false; %plot shooting method progress
        shoot.plot_ode = false; %plot ode progress (very slow)
    else
        bvp_plot = false; %currently only does anything with homeade bvp solver
    end
    slices = true; %slices rather than full grid
    sgrid.slices = slices;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% set up grid search
if ~slices
    numgridel = prod(sgrid.dim);
    if ~isempty(sgrid.var_name)
        sgrid.dim = NaN(size(sgrid.var_name));
        for i = 1:length(sgrid.dim)
            sgrid.dim(i) = length(sgrid.var_val{i});
        end
    else
        %just do one run
        sgrid.dim = 1;
    end
    %separate grids for parallell for loop (not optimal way to do this)
    shootval_grid_cell = cell(prod(sgrid.dim),1);
    sgrid.shootval_grid = zeros(sgrid.dim);
    phiend_grid_cell = cell(prod(sgrid.dim),1);
    phimax_grid_cell = cell(prod(sgrid.dim),1);
    u_gfend_grid_cell = cell(prod(sgrid.dim),1);
    u_gfmax_grid_cell = cell(prod(sgrid.dim),1);
    w_fmax_grid_cell = cell(prod(sgrid.dim),1);
    w_fend_grid_cell = cell(prod(sgrid.dim),1);
    d_f_grid_cell = cell(prod(sgrid.dim),1);
    pstruct_grid_cell = cell(prod(sgrid.dim),1);
    sgrid.phiend_grid = zeros(sgrid.dim);
    sgrid.phimax_grid = zeros(sgrid.dim);
    sgrid.u_gfend_grid = zeros(sgrid.dim);
    sgrid.u_gfmax_grid = zeros(sgrid.dim);
    sgrid.w_fmax_grid = zeros(sgrid.dim);
    sgrid.w_fend_grid = zeros(sgrid.dim);
    sgrid.d_f_grid = zeros(sgrid.dim);
    if save_sols
        sol_grid = cell(sgrid.dim);
        sol_grid_cell = cell(prod(sgrid.dim),1);
    end
    sgrid.pstruct_grid = cell(sgrid.dim);
else
    numslices = sum(1:length(sgrid.var_name)-1);
    P0_cell = cell(numslices,1);
    shootval_cell = cell(numslices,1);
    phiend_cell = cell(numslices,1);
    phimax_cell = cell(numslices,1);
    u_gfend_cell = cell(numslices,1);
    u_gfmax_cell = cell(numslices,1);
    w_fmax_cell = cell(numslices,1);
    w_fend_cell = cell(numslices,1);
    int_q_w_end_cell = cell(numslices,1);
    q_gf_end_cell = cell(numslices,1);
    q_gb_end_cell = cell(numslices,1);
    q_gf_max_cell = cell(numslices,1);
    d_perc_cell = cell(numslices,1);
    d_f_cell = cell(numslices,1);
%     pstruct_cell = cell(numslices,1);
    sgrid.var1_ind_cell = {};
    sgrid.var2_ind_cell = {};
    sgrid.var1_val_cell = {};
    sgrid.var2_val_cell = {};
    sgrid.var1_name_cell = {};
    sgrid.var2_name_cell = {};
    sgrid.var1_text_cell = {};
    sgrid.var2_text_cell = {};
    numgridel = 0;
    startinds = ones(size(numslices));
    count = 0;
    for v1 = 1:length(sgrid.var_name)
        for v2 = 1:v1-1
            sgrid.var1_ind_cell{end+1} = v2;
            sgrid.var2_ind_cell{end+1} = v1;
            sgrid.var1_val_cell{end+1} = sgrid.var_val{v2};
            sgrid.var2_val_cell{end+1} = sgrid.var_val{v1};
            sgrid.var1_name_cell{end+1} = sgrid.var_name{v2};
            sgrid.var2_name_cell{end+1} = sgrid.var_name{v1};
            sgrid.var1_text_cell{end+1} = sgrid.var_text{v2};
            sgrid.var2_text_cell{end+1} = sgrid.var_text{v1};
            count = count + 1;
            startinds(count) = numgridel + 1;
            numgridel = numgridel + length(sgrid.var_val{v2})*length(sgrid.var_val{v1});
        end
    end
end

warning('off','MATLAB:mir_warning_maybe_uninitialized_temporary')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%PARFOR
% change to "for" to show plots, "parfor" to run in parallell
'not using parfor'
numcores = feature('numcores'); 
poolobj = gcp('nocreate'); 
if isempty(poolobj)
    parpool(numcores)
end
totalstart = tic;
%for sgrid_linind = 1:numgridel %loop through grid
for slicein = 1:numslices
 P0_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 shootval_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 phiend_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 phimax_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 u_gfend_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 u_gfmax_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 w_fmax_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 w_fend_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 int_q_w_end_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 q_gf_end_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 q_gb_end_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 q_gf_max_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 d_perc_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 d_f_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 % pstruct_cell{slicein} = zeros(length(sgrid.var1_val_cell{slicein}),length(sgrid.var2_val_cell{slicein}));
 for v1in = 1:length(sgrid.var1_val_cell{slicein})
  for v2in = 1:length(sgrid.var2_val_cell{slicein})
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %setup grids for use in parfor loop
    pstruct = main_pstruct;
    if ~slices
        if ~isempty(sgrid.var_name)
            sgrid_vecind = cell(size(sgrid.dim));
            [sgrid_vecind{:}] = ind2sub(sgrid.dim,sgrid_linind);
            for i = 1:length(sgrid.var_name)
                pstruct.(sgrid.var_name{i}) = sgrid.var_val{i}(sgrid_vecind{i});
            end
            disp(sgrid_vecind)
        end
    else
        if ~isempty(sgrid.var_name)
            for i = 1:length(sgrid.var_name)
                pstruct.(sgrid.var_name{i}) = sgrid.var_default{i};
            end
            pstruct.(sgrid.var1_name_cell{slicein}) = sgrid.var1_val_cell{slicein}(v1in);
            pstruct.(sgrid.var2_name_cell{slicein}) = sgrid.var2_val_cell{slicein}(v2in);
        end
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if use_bvp
        %% bvp technical parameters
        bvp = struct();

        %initial guesses
        bvp.r_base_init = 3;
        bvp.phiend_init = 1E-3;

        bvp.bvpoptions = bvpset('RelTol',1E-6,...
            'AbsTol',1E-8,...
            'NMax',1E6,...
            'Stats','on');
        bvp.NInit = 1E4; %inital mesh resolution

    %     bvp.use_josh_bvp_fun = false;
    %     %use homeade bvp solver
    %     if bvp.use_josh_bvp_fun
    %         %max number of iterations per grid
    %         bvp.bvpoptions.MaxIter = 1E4;
    %         %max number of grid iterations
    %         bvp.bvpoptions.MaxGridIter = 2;
    %         %force values to stay within bounds
    %         bvp.bvpoptions.LBound = @(z) [0*z + pstruct.Patm; 0*z + 1E-6];
    %         [n_d_min, ~] = solubility_fun(pstruct, 4*pstruct.Patm);
    %         bvp.mu_max = mu_m_fun(pstruct,...
    %             n_d_min,...
    %             pstruct.Xtal);
    %         bvp.Pmax = pstruct.L*(pstruct.q*8*bvp.mu_max...
    %             /(bvp.rho_min*pi*bvp.r_c^4) + bvp.rho_max*pstruct.g) + pstruct.Patm;
    %         bvp.bvpoptions.UBound = @(z) [pstruct.Patm ...
    %             + (pstruct.Patm - bvp.Pmax)/pstruct.L*z;...
    %             0*z + 1 - 1E-6];
    %         %plot results with each iteration
    %         bvp.bvpoptions.Plot = bvp_plot;
    %         %damping scale
    %         bvp.bvpoptions.Damping = 1E-1;
    %     end

    else
        %% shooting technical parameters
        
        shootlocal = shoot;

        %secant method should be faster in some cases, but in some cases such
        %as with very nonlinear cost function is much slower, so for geberal
        %param search safest to set to 2 or greater so that won't be used every
        %step
        shootlocal.secant_inc = 3;

        if shootlocal.plot_ode
            shootlocal.ode_outputfunction = @(z, var_vec, flag, varargin)...
                odeplot(z, real(var_vec), flag, varargin);
        else
            shootlocal.ode_outputfunction = [];
        end

        %maximum z to integrate to in shooting method
        shootlocal.z_max = 40;

        %set bounds for shooting method
        if strcmp(shootlocal.shootvar, 'q')
            %q bounds for shooting
            %poisiulle flow estimate for total mass flux bounds
            shootlocal.rho_max = pstruct.rho_m;
            shootlocal.rho_min = 0.1*pstruct.rho_m;

            shootlocal.mu_min = mu_m_fun(pstruct,...
                pstruct.n_v0,...
                pstruct.Xtal);
            [n_d_min, ~] = solubility_fun(pstruct, 4*pstruct.Patm);
            shootlocal.mu_max = mu_m_fun(pstruct,...
                n_d_min,...
                pstruct.Xtal);

            shootlocal.lower_bound = ((pstruct.P0 - pstruct.Patm)/pstruct.L - shootlocal.rho_max*pstruct.g)...
                *shootlocal.rho_min*pi*pstruct.r_c0^4/(8*shootlocal.mu_max);
            if pstruct.lower_bound <= 0
                %needed if P0 was set too low
                shootlocal.rho_max_scale = 1 - 1E-4;
                shootlocal.lower_bound = ((pstruct.P0 - pstruct.Patm)/pstruct.L ...
                    - shootlocal.rho_max_scale*shootlocal.rho_max*pstruct.g)*shootlocal.rho_min*pi*pstruct.r_c0^4 ...
                    /(8*shootlocal.mu_max);
            end
            shootlocal.upper_bound = ((pstruct.P0 - pstruct.Patm)/pstruct.L - shootlocal.rho_min*pstruct.g)...
                *shootlocal.rho_max*pi*pstruct.r_c0^4/(8*shootlocal.mu_min);

            shootlocal.min_step = 1E-6*shootlocal.q_lower_bound; %min q step

        elseif strcmp(shootlocal.shootvar, 'P0')
            %P0 bounds for shooting
            %poisiulle flow estimate for pressure bounds
            shootlocal.rho_max = pstruct.rho_m;
            shootlocal.rho_min = 0.01*pstruct.rho_m;

            shootlocal.mu_min = mu_m_fun(pstruct,...
                pstruct.n_v0,...
                pstruct.Xtal);
            [n_d_min, ~] = solubility_fun(pstruct, shootlocal.rho_min*9.8*pstruct.L/2);
            shootlocal.mu_max = mu_m_fun(pstruct,...
                n_d_min,...
                pstruct.Xtal);
            shootlocal.lower_bound = pstruct.L*(pstruct.q*8*shootlocal.mu_min...
                /(shootlocal.rho_max*pi*pstruct.r_c0^4) + shootlocal.rho_min*pstruct.g) + pstruct.Patm;     
            shootlocal.upper_bound = pstruct.L*(pstruct.q*8*shootlocal.mu_max...
                /(shootlocal.rho_min*pi*pstruct.r_c0^4) + shootlocal.rho_max*pstruct.g) + pstruct.Patm;

            shootlocal.min_step = 1E4; %min P0 step (for NaN bounded cases)
            
        elseif strcmp(shootlocal.shootvar, 'r_base')
            %r bounds for shooting
            %poisiulle flow estimate for pressure bounds
            shootlocal.rho_max = pstruct.rho_m;
            shootlocal.rho_min = 0.4*pstruct.rho_m;

            shootlocal.mu_min = mu_m_fun(pstruct,...
                pstruct.n_v0,...
                pstruct.Xtal);
            [n_d_min, ~] = solubility_fun(pstruct, shootlocal.rho_min*9.8*pstruct.L/2);
            shootlocal.mu_max = mu_m_fun(pstruct,...
                n_d_min,...
                pstruct.Xtal);
            shootlocal.lower_bound = ((pstruct.q*2*shootlocal.mu_min)/...
                (shootlocal.rho_max*pi*((pstruct.P0 - pstruct.Patm)/pstruct.L + shootlocal.rho_max*pstruct.g))).^0.25;     
            shootlocal.upper_bound = ((pstruct.q*10*shootlocal.mu_max)/...
                (shootlocal.rho_min*pi*((pstruct.P0 - pstruct.Patm)/pstruct.L + shootlocal.rho_min*pstruct.g))).^0.25;  
            shootlocal.min_step = 2E-9; %min r step (for NaN bounded cases)

%             'setting different shoot bounds'
%             shootlocal.lower_bound = 2.5770;
%             shootlocal.upper_bound = 2.5785;
            
        end

        %top pressure error tolerance
        shootlocal.shoot_tol = 6E0; %m
        shootlocal.shoot_max_iter = 2E3; %max iterations
        shootlocal.target_xend = 0; %target end depth
        
        shootlocal.ode_solver = '23s';
        %23s seems to work best
        %23t also seems to work ok, but might be less general
        %23tb works but slow
        %15s works but slow
        %113 might work but very slow and large oscillations
        %23 might work but very slow and large oscillations
        %45 might work but very slow and large oscillations

        %Don't think Jacobian can be calculated more efficiently than with built in
        %finite difference estimation
        %haven't tried vectorizing
        if main_pstruct.use_k_w
            abs_tol_vec = [5e-3,5e-11,1e-1];
        else
            abs_tol_vec = [5e-3,5e-11];
        end
        shootlocal.ode_options = odeset('AbsTol', abs_tol_vec,...
            'RelTol', 5e-8,...
            'NormControl','off',...
            'OutputFcn',shootlocal.ode_outputfunction,...
            'Stats','off',...
            'InitialStep',1e-7,...
            'MaxStep',1E-1,...
            'Vectorized', 'off');%'NonNegative',[1,2]);
        

    end

    %% other technical parameters

    %for finding relative gas velocities by minimizing force
    % pstruct.fminbnd_options = optimset('FunValCheck','on',...
    %     'TolX',1e-8,...
    %     'TolFun',1E-8);

    pstruct.use_fixed_u_gb_preperc = false; %fix u_gb == u_m prior to percolation
    %setting u_gb = u_m when phi<phi_pL prevents oscillatory u_gb, 
    %so greatly speeds up computation but neglects bouyancy
    %Also tried solving for pre-permeability flow analytically but doesn't
    %seem feasible. Could however further simplify numerics in this regime
    %(should only need to solve for P), but wouldn't make much difference
    %in efficiency and wouldn't help numerical issues at transition. Also
    %tried various ways for damping velocities to minimize oscillations,
    %some helped but might not fix transition issues and at that point
    %might as well just take this approach of setting u_gb = u_m.
    %best option just seems to be setting percolation thresholds negative,
    %so as to not deal with stokes regime or transition.

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% SETUP
    %integration interval
    if use_bvp
        bvp.zspan = [-pstruct.L,0];
    else
        shootlocal.zspan = [-pstruct.L,shootlocal.z_max];
    end

    %initial conditions
    if use_bvp
        pstruct.q_l = pstruct.q*(1 - pstruct.n_v0); %non-volatile flux
        pstruct.q_v = pstruct.q*pstruct.n_v0; %volatile flux (exsolved+dissolved)
        bvp.P0_init = pstruct.P0;
    else
        shootlocal.shoot_IC_fun = @(pstruct_in,shoot_in,shootvar_val) ...
            shoot_IC_fun_minphi(pstruct_in,shoot_in,shootvar_val);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if use_bvp
        %% bvp solver (relaxation method)

        %%calculate initial phi guess
        bvp.rho_g0_init = bvp.P0_init/(pstruct.gasConst*pstruct.Temp);
        [n_d0_init, ~] = solubility_fun(pstruct, bvp.P0_init);
        bvp.n_d0_init = n_d0_init;
        bvp.n_g0_init = (pstruct.n_v0 - bvp.n_d0_init)/(1 - bvp.n_d0_init);
        if bvp.n_g0_init < 0
            bvp.n_g0_init = 0;
        end
        %assuming no fractures initially, and no gas flux
        %!!!not properly accounting for crystals!!!!
        bvp.phi0_init = (bvp.n_g0_init/bvp.rho_g0_init)...
            /(bvp.n_g0_init/bvp.rho_g0_init + (1 - bvp.n_g0_init)/pstruct.rho_m);

        %%initial parameter guesses
        bvp.param_vec_init = [bvp.r_base_init];
        pstruct.param_vec_names = {'r_base'};
        
        %%initial conditions, assuming both linear
        bvp.yinit = @(z) [z*(pstruct.Patm - bvp.P0_init)/pstruct.L + pstruct.Patm;...
            z*(bvp.phiend_init - bvp.phi0_init)/pstruct.L + bvp.phiend_init];
        bvp.solinit = bvpinit(linspace(zspan(1),zspan(2),bvp.NInit), bvp.yinit,...
            bvp.param_vec_init);

        %solve bvp
    %     if bvp.use_josh_bvp_fun
    %     odeSolution = bvp_josh_fun(@(z,var_vec) odeEquations_effectiveK(z,var_vec,pstruct),...
    %         @(ya,yb) bcfun_effectiveK_josh(ya,yb,pstruct),...
    %         bvp.solinit, bvp.bvpoptions);
    %     else
            odeSolution = bvp5c(@(z, var_vec, param_vec) odeEquations_minphi(z, var_vec, param_vec, pstruct),...
            @(ya, yb, param_vec) bcfun_effectiveK(ya, yb, param_vec, pstruct),...
            bvp.solinit, bvp.bvpoptions);
    %     end
        if odeSolution.error
            sol_converged = true;
        else
            sol_converged = false;
        end

    else
        %% Shooting Method
        if ~slices
%             disptext = strcat('case',num2str(sgrid_linind),'-out of-',num2str(prod(sgrid.dim)));
        else
            disptext = strcat('_',num2str(slicein),'_',num2str(v1in),'_',num2str(v2in),'_');
        end
        [odeSolution, pstruct, sol_converged, shootvar_val] = ...
            shoot_fun_minphi(pstruct, shootlocal, disptext);
        if sol_converged == false
            disp('Shooting Not Converge in allowed iterations')
        end
    
    end

    if sol_converged
        %% update grids
        if ~isempty(sgrid.var_name)
            %add to grid
            if ~slices
%                 if use_bvp
%                     P0_grid_cell{sgrid_linind}(sgrid_linind) = odeSolution.y(1,1);
%                 else
%                     shootval_grid_cell{sgrid_linind} = shootvar_val;
%                 end
%                 phiend_grid_cell{sgrid_linind} = odeSolution.y(2,end);
%                 phimax_grid_cell{sgrid_linind} = max(odeSolution.y(2,:));
%                 if save_sols
%                     sol_grid_cell{sgrid_linind} = odeSolution;
%                 end
%                 pstruct_grid_cell{sgrid_linind} = pstruct;
            else
                if use_bvp
                    P0_cell{slicein}(v1in,v2in) = odeSolution.y(1,1);
                else
                    shootval_cell{slicein}(v1in,v2in) = shootvar_val;
                end
                phiend_cell{slicein}(v1in,v2in) = odeSolution.y(2,end);
                phimax_cell{slicein}(v1in,v2in) = max(odeSolution.y(2,:));
%                 pstruct_cell{slicein}(v1in,v2in) = pstruct;
            end
        end

        %                       
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% post calculations
        if post_calc || plot_depth_profiles
            out = postcalc_minphi(pstruct,odeSolution);
            if ~slices
%                 u_gfmax_grid_cell{sgrid_linind} = nanmax(out.u_gf);
%                 u_gfend_grid_cell{sgrid_linind} = out.u_gf(end);
%                 w_fend_grid_cell{sgrid_linind} = out.w_f(end);
%                 w_fmax_grid_cell{sgrid_linind} = nanmax(out.w_f);
%                 f_in = find(~isnan(out.w_f) & out.w_f > 0, 1);
%                 if isempty(f_in)
%                     d_f_grid_cell{sgrid_linind} = NaN;
%                 else
%                     d_f_grid_cell{sgrid_linind} = out.z(f_in);
%                 end
            else
                u_gfmax_cell{slicein}(v1in,v2in) = nanmax(out.u_gf);
                u_gfend_cell{slicein}(v1in,v2in) = out.u_gf(end);
                w_fend_cell{slicein}(v1in,v2in) = out.w_f(end);
                w_fmax_cell{slicein}(v1in,v2in) = nanmax(out.w_f);
                int_q_w_end_cell{slicein}(v1in,v2in) = out.int_q_w(end);
                q_gf_end_cell{slicein}(v1in,v2in) = out.q_gf(end);
                q_gb_end_cell{slicein}(v1in,v2in) = out.q_gb(end);
                q_gf_max_cell{slicein}(v1in,v2in) = max(out.q_gf/(out.q_gf + out.q_gb));            
                f_in = find(out.phi > pstruct.perm_phi_crit, 1);
                if isempty(f_in)
                    d_perc_cell{slicein}(v1in,v2in) = NaN;
                else
                    d_perc_cell{slicein}(v1in,v2in) = out.z(f_in);
                end
                f_in = find(~isnan(out.w_f) & out.w_f > 0, 1);
                if isempty(f_in)
                    d_f_cell{slicein}(v1in,v2in) = NaN;
                else
                    d_f_cell{slicein}(v1in,v2in) = out.z(f_in);
                end
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% plot
        if plot_depth_profiles   
            fhandle = plot_depthprofiles_effectiveK(pstruct,out);
        end

    else
        if ~slices
%             phiend_grid_cell{sgrid_linind} = NaN;
%             phimax_grid_cell{sgrid_linind} = NaN;
%             u_gfend_grid_cell{sgrid_linind} = NaN;
%             u_gfmax_grid_cell{sgrid_linind} = NaN;
%             w_fmax_grid_cell{sgrid_linind} = NaN;
%             w_fend_grid_cell{sgrid_linind} = NaN;
%             d_f_grid_cell{sgrid_linind} = NaN;
%             if save_sols
%                 sol_grid_cell{sgrid_linind} = struct();
%             end
%             pstruct_grid_cell{sgrid_linind} = pstruct;
%             shootval_grid_cell{sgrid_linind} = NaN;
        else
            phiend_cell{slicein}(v1in,v2in) = NaN;
            phimax_cell{slicein}(v1in,v2in) = NaN;
            u_gfend_cell{slicein}(v1in,v2in) = NaN;
            u_gfmax_cell{slicein}(v1in,v2in) = NaN;
            w_fmax_cell{slicein}(v1in,v2in) = NaN;
            w_fend_cell{slicein}(v1in,v2in) = NaN;
            d_f_cell{slicein}(v1in,v2in) = NaN;
%             pstruct_cell{slicein}(v1in,v2in) = pstruct;
            shootval_cell{slicein}(v1in,v2in) = NaN;
        end
    end %end to if solution converged
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  end
 end
end %end to grid search


%% combine and save results
warning('on','MATLAB:mir_warning_maybe_uninitialized_temporary')
if ~isempty(sgrid.var_name)
    if ~slices
%         %recombine values
%         for sgrid_linind = 1:prod(sgrid.dim)
%             sgrid.shootval_grid = sgrid.shootval_grid + shootval_grid_cell{sgrid_linind};
%             sgrid.phiend_grid(sgrid_linind) = phiend_grid_cell{sgrid_linind};
%             sgrid.phimax_grid(sgrid_linind) = phimax_grid_cell{sgrid_linind};
%             sgrid.u_gfend_grid(sgrid_linind) = u_gfend_grid_cell{sgrid_linind};
%             sgrid.u_gfmax_grid(sgrid_linind) = u_gfmax_grid_cell{sgrid_linind};
%             sgrid.w_fmax_grid(sgrid_linind) = w_fmax_grid_cell{sgrid_linind};
%             sgrid.w_fend_grid(sgrid_linind) = w_fend_grid_cell{sgrid_linind};
%             sgrid.d_f_grid(sgrid_linind) = d_f_grid_cell{sgrid_linind};
%             if save_sols
%                 sol_grid{sgrid_linind} = sol_grid_cell{sgrid_linind};
%             end
%             sgrid.pstruct_grid{sgrid_linind} = pstruct_grid_cell{sgrid_linind};
%         end
    else
        sgrid.shootval_cell = shootval_cell;
        sgrid.phiend_cell = phiend_cell;
        sgrid.phimax_cell = phimax_cell;
        sgrid.u_gfend_cell = u_gfend_cell;
        sgrid.u_gfmax_cell = u_gfmax_cell;
        sgrid.w_fmax_cell = w_fmax_cell;
        sgrid.w_fend_cell = w_fend_cell;
        sgrid.d_f_cell = d_f_cell;
%         sgrid.pstruct_cell = pstruct_cell;
    end
end

disp('grid search finished')

if save_grid
    FileName = 'minphi_sgrid_equalF_wallperm.mat';
    if slices
        FileName = strcat('slices_',FileName);
    end
    [fPath, fName, fExt] = fileparts(FileName);
    if isempty(fExt)  % No '.mat' in FileName
      fExt     = '.mat';
      FileName = fullfile(fPath, [fName, fExt]);
    end
    if exist(FileName, 'file')
      % Get number of files:
      fDir     = dir(fullfile(fPath, [fName, '*', fExt]));
      fStr     = lower(sprintf('%s*', fDir.name));
      fNum     = sscanf(fStr, [fName, '%d', fExt, '*']);
      newNum   = max(fNum) + 1;
      FileName = fullfile(fPath, [fName, sprintf('%d', newNum), fExt]);
    end
    sgrid.pstruct = main_pstruct;
    save(FileName, 'sgrid','-v7.3')
    if save_sols
        save(strcat(FileName,'_sols'), 'sol_grid','-v7.3')
    end
end

toc(totalstart)

