function fhandle = plot_depthprofiles_effectiveK(pstruct,out)
%plot depth profiles
%josh crozier 2019

fhandle = figure('Name','depth profiles');

Ylim = [-2000,0];

numplots = 6;
t = tiledlayout(1,numplots);
t.TileSpacing = 'compact';

%plot p(depth)
nexttile
plot(1e-6*out.P_m,out.z,'k','LineWidth',2), hold on
plot(1e-6*out.P_lith,out.z,'-','LineWidth',1,'color',[0.9290 0.6940 0.1250]), hold on
legend({'Magma','Lithostatic'},'Location','southwest')
set(gca,'FontSize',12,'XScale','linear')
%axis ij
%axis([0 1.3e2 -200 4000])
%set(gca,'XTick',[0 0.5e2 1e2])
xlabel('Pressure (MPa)','FontSize',12);
ylabel('Elevation (m)','FontSize',12);
grid on
grid minor
ylim(Ylim)
%title(strcat('r_base = ',{' '},num2str(pstruct.r_base,'%.2f'),' m'))

%plot n_d(depth)
nexttile
plot(100*out.n_d,out.z,'r','LineWidth',2), hold on
set(gca,'FontSize',12,'XScale','linear')
%axis ij
%axis([0 1.3e2 -200 4000])
%set(gca,'XTick',[0 0.5e2 1e2])
xlabel('Dissolved Gas (wt%)','FontSize',12);
grid on
grid minor
ylim(Ylim)
set(gca,'YTickLabel',[])

%plot phi(depth)
nexttile
plot(out.phi_b,out.z,'b','LineWidth',2), hold on 
plot(out.phi_br,out.z,':','LineWidth',0.75,'Color',[0,0,1]), hold on 
out.phi_f(out.phi_f == 0) = NaN;
plot(out.phi_f,out.z,'c','LineWidth',2), hold on
plot(out.phi,out.z,'k:','LineWidth',2), hold on 
plot(out.phi_target,out.z,'k-','LineWidth',0.5), hold on 
%axis ij
% set(gca,'XTick',[0 20 40 60 80])
set(gca,'YTickLabel',[])
%axis([0 100.0 0 140])
%axis([0 80.0 -200 4000])
%set(gca,'FontSize',12,'XScale','linear','Yscale','linear')
set(gca,'FontSize',12,'XScale','linear')
%ylabel('Pressure (MPa)','FontSize',12);
xlabel('Volume Fraction','FontSize',12);
legend({'Bubbles','Bubbles \newline (relative)','Fractures','Total','Threshold'},...
    'Location','southwest')
grid on
grid minor
ylim(Ylim)

%plot widths
nexttile
% plot(u_m.*pstruct.rho_m.*(A - A_f).*(1 - phi),...
%     z,'g','LineWidth',2),
plot(sqrt(out.A/pi)*2, out.z,'k','LineWidth',2), hold on
% out.r_b = (3*out.phi_br./(4*pi*pstruct.N_b*(1 - out.phi_br))).^(1/3);
% plot(2*out.r_b, out.z,'b','LineWidth',2), hold on
out.w_f(out.w_f == 0) = NaN;
plot(out.w_f, out.z,'c','LineWidth',2), hold on
%plot(q_w_gb, z,'m:','LineWidth',2), hold on
set(gca,'FontSize',12,'XScale','log')
%axis ij
%set(gca,'XTick',[1e5 1e6 1e7])
set(gca,'YTickLabel',[])
%ylim([-200 4000])
%xlim([1E2,1E10])
xlabel('Width (m)','FontSize',12);
legend({'Conduit','Fractures'},...
    'Location','south')
% legend({'Total Gas','Gas in Bubbles','Gas in Fractures','Lateral Gas'},...
%     'Location','south')
grid on
grid minor
ylim(Ylim)
xticks(10.^(-7:1))

%plot v(depth)
nexttile
plot(out.u_m,out.z,'r','LineWidth',2), hold on
plot(out.u_gb,out.z,'b','LineWidth',2)
plot(out.u_gf,out.z,'c','LineWidth',2)
plot(out.u_g_avg,out.z,':','Color','k','LineWidth',2)
%plot(w_gb,z,':m','LineWidth',2)
%axis ij
%axis([0.005 50 -200 4000])
% set(gca,'XTick',[1e-2 1e-1 1 10])
set(gca,'YTickLabel',[])
set(gca,'FontSize',12,'XScale','log')
xlabel('Velocity (m/s)','FontSize',12);
legend({'Melt','Bubbles','Fractures', 'Gas avg.'},...
    'Location','southeast')
% legend({'Melt','Gas in Bubbles','Gas in Fractures', 'Lateral Gas'})
grid on
grid minor
ylim(Ylim)
xticks([(1)*1E-2,(1)*1E-1,(1)*1E0,(1)*1E1,(1)*1E2])

%plot mass flux(depth)
nexttile
% plot(u_m.*pstruct.rho_m.*(A - A_f).*(1 - phi),...
%     z,'g','LineWidth',2),
plot(out.q_gb, out.z,'b','LineWidth',2), hold on
out.q_gf(out.q_gf == 0) = NaN;
plot(out.q_gf, out.z,'c','LineWidth',2), hold on
plot(out.q_g, out.z,'k:','LineWidth',2), hold on
plot(out.q_w, out.z,':','LineWidth',2,'color',[0.9290 0.6940 0.1250]), hold on
plot(out.int_q_w, out.z,'-','LineWidth',1.0,'color',[0.9290 0.6940 0.1250]), hold on
%plot(q_w_gb, z,'m:','LineWidth',2), hold on
set(gca,'FontSize',12,'XScale','linear')
%axis ij
%set(gca,'XTick',[1e5 1e6 1e7])
set(gca,'YTickLabel',[])
%ylim([-200 4000])
%xlim([1E2,1E10])
xlabel('Gas Flux (kg/s)','FontSize',12);
legend({'Bubbles','Fractures','Total','Wall Rock\newline(lateral)','Wall Rock\newline(cumulative)'},...
    'Location','south')
% legend({'Total Gas','Gas in Bubbles','Gas in Fractures','Lateral Gas'},...
%     'Location','south')
grid on
grid minor
ylim(Ylim)

%         %plot lateral mass flux(depth)
%         subplot(1,6,7)
%         plot(q_w_gb, z,'c','LineWidth',2), hold on
%         set(gca,'FontSize',12,'XScale','log')
%         %axis ij
%         %set(gca,'XTick',[1e5 1e6 1e7])
%         % set(gca,'YTickLabel',[])
%         %ylim([-200 4000])
%         %xlim([1E2,1E10])
%         xlabel('Mass Flux (kg/s)','FontSize',12);
%         legend({'Lateral Gas'},...
%              'Location','south')
%         grid on
%         ylim(Ylim)

% %plot costfun
% subplot(1,numplots,7)
% plot(out.cost,out.z,'k','LineWidth',2), hold on
% set(gca,'FontSize',12,'XScale','linear')
% %axis ij
% %axis([0 1.3e2 -200 4000])
% %set(gca,'XTick',[0 0.5e2 1e2])
% xlabel('cost','FontSize',12);
% grid on
% grid minoroik,
% ylim(Ylim)
% set(gca,'YTickLabel',[])

% %plot k
% subplot(1,numplots,7)
% plot(out.k_1,out.z,'k','LineWidth',2), hold on
% plot(out.k_2,out.z,'k--','LineWidth',2)
% %plot(w_gb,z,':m','LineWidth',2)
% %axis ij
% %axis([0.005 50 -200 4000])
% % set(gca,'XTick',[1e-2 1e-1 1 10])
% set(gca,'YTickLabel',[])
% set(gca,'FontSize',12,'XScale','log')
% xlabel('K (m^2)','FontSize',12);
% legend({'K_1','K_2'},...
%     'Location','southeast')
% % legend({'Melt','Gas in Bubbles','Gas in Fractures', 'Lateral Gas'})
% grid on
% grid minor
% ylim(Ylim)

% %plot Forces
% subplot(1,numplots,8)
% plot(out.F_mgb,out.z,'r','LineWidth',2), hold on
% plot(out.F_mgf,out.z,':b','LineWidth',2), hold on
% plot(out.F_mg_total,out.z,'k--','LineWidth',2), hold on
% set(gca,'FontSize',12,'XScale','log')
% %axis ij
% %set(gca,'XTick',[1e5 1e6 1e7])
% set(gca,'YTickLabel',[])
% %ylim([-200 4000])
% % xlim([min([out.F_mgf(out.z > out.z(1) + 20 & out.F_mgf > 0),...
% %     out.F_mgb(out.z > out.z(1) + 20 & out.F_mgb > 0)]),...
% %     max([out.F_mgf,out.F_mgb])])
% xlabel('Force (N)','FontSize',12);
% legend({'Bubble','Fracture','Total'},'Location','south')
% grid on
% ylim(Ylim)

end

