function [dP_m_dz, dphi_dz, du_m_dz, du_g_dz] = ...
    dvar_dz_vec_effectiveK_fun(pstruct,...
        phi,...
        rho_g,...
        u_g_avg,...
        drho_g_dP_coeff,...
        u_m,...
        dn_d_dP_coeff,...
        F_mg_total,...
        F_mw,...
        A,...
        dA_dz,...
        q_w)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%set up for solving conservation equations
%josh crozier 2019


g = pstruct.g;
rho_m = pstruct.rho_m;
use_fixed_u_gb_preperc = pstruct.use_fixed_u_gb_preperc;
phi_pL = pstruct.phi_pL;
%phi_pU = pstruct.phi_pU;


if phi > phi_pL || ~use_fixed_u_gb_preperc

    %[var1=dP_m_dz, var2=dphi_dz, var3=du_m_dz, var4=du_g_dz]
    const_vec = zeros(4,1);

    % eq1: gas mass conservation
    %-phi*rho_g*u_g_avg*dA_dz -q_w = 
    %+ phi*u_gb*(A)*drho_g_dz
    %+ (A)*(1 - phi)*rho_m*u_m*dn_d_dz
    %+ rho_g*u_gb*(A)*dphi_dz
    %+ phi*rho_g*(A)*du_g_dz

    const_vec(1) = -phi*rho_g*u_g_avg*dA_dz - q_w;
    g_mass_con_coeff_vec = zeros(1,4);
    %var1=dP_dz
    g_mass_con_coeff_vec(1) = ...
        phi*u_g_avg*(A)*drho_g_dP_coeff...
        + (A)*(1 - phi)*rho_m*u_m*dn_d_dP_coeff;
    %var2=dphi_dz
    g_mass_con_coeff_vec(2) = rho_g*u_g_avg*(A);
    %var3=du_m_dz
    g_mass_con_coeff_vec(3) = 0;
    %var4=du_g_dz
    g_mass_con_coeff_vec(4) = phi*rho_g*(A);


    % eq2: melt mass conservation
    %- (1 - phi)*rho_m*u_m*dA_dz =
    %- (A)*(1 - phi)*rho_m*u_m*dn_d_dz
    %- rho_m*u_m*(A)*dphi_dz
    %+ (1 - phi)*rho_m*(A)*du_m_dz

    const_vec(2) = - (1 - phi)*rho_m*u_m*dA_dz;
    m_mass_con_coeff_vec = zeros(1,4);
    %var1=dP_dz
    m_mass_con_coeff_vec(1) = -(A)*(1 - phi)*rho_m*u_m*dn_d_dP_coeff;
    %var2=dphi_dz
    m_mass_con_coeff_vec(2) = -rho_m*u_m*(A);
    %var3=du_m_dz
    m_mass_con_coeff_vec(3) = (1 - phi)*rho_m*(A);
    %var4=du_g_dz
    m_mass_con_coeff_vec(4) = 0;


    % eq3: melt momentum conservation
    %- F_mgb_eff + F_mw + (A)*(1-phi)*rho_m*g = ...
    %-(A)*(1-phi)*dP_dz...
    %-(A)*(1-phi)*rho_m*u_m*du_m_dz;

    
    const_vec(3) = - F_mg_total + F_mw + (A)*(1-phi)*rho_m*g;
    m_mom_con_coeff_vec = zeros(1,4);
    %var1=dP_dz
    m_mom_con_coeff_vec(1) = -(A)*(1-phi);
    %var2=dphi_dz
    m_mom_con_coeff_vec(2) = 0;
    %var3=du_m_dz
    m_mom_con_coeff_vec(3) = -(A)*(1-phi)*rho_m*u_m;
    %var4=du_g_dz
    m_mom_con_coeff_vec(4) = 0;


    % eq4: gas in bubble momentum conservation
    %F_mgb + (A)*phi*rho_g*g = ...
    % -(A)*phi*dP_dz...
    %- (A)*phi*rho_g*u_gb*du_g_dz;

    const_vec(4) = F_mg_total + (A)*phi*rho_g*g;
    gb_mom_con_coeff_vec = zeros(1,4);
    %var1=dP_dz
    gb_mom_con_coeff_vec(1) = -(A)*phi;
    %var2=dphi_dz
    gb_mom_con_coeff_vec(2) = 0;
    %var3=du_m_dz
    gb_mom_con_coeff_vec(3) = 0;
    %var4=du_g_dz
    gb_mom_con_coeff_vec(4) = -(A)*phi*rho_g*u_g_avg;


    % solve for derivatives
    Coeff_mat = [g_mass_con_coeff_vec;...
        m_mass_con_coeff_vec;...
        m_mom_con_coeff_vec;...
        gb_mom_con_coeff_vec];

    warning('off','MATLAB:nearlySingularMatrix')
    dvar_dz_vec = Coeff_mat\const_vec;
    warning('on','MATLAB:nearlySingularMatrix')
    
%     if ~isfinite(rcond(Coeff_mat)) || abs(rcond(Coeff_mat)) < eps
%         debug pause
%     end
    
    %[var1=dP_dz, var2=dphi_dz, var3=du_m_dz, var4=du_g_dz]
    dP_m_dz = dvar_dz_vec(1);
    dphi_dz = dvar_dz_vec(2);
    du_m_dz = dvar_dz_vec(3);
    du_g_dz = dvar_dz_vec(4);


else
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %setting u_gb = u_m when phi<phi_pL prevents oscillatory u_gb, 
    %so greatly speeds up computation but neglects bouyancy
    %Also tried solving for pre-permeability flow analytically but doesn't
    %seem feasible. Could however further simplify numerics in this regime
    %(should only need to solve for P), but wouldn't make much difference
    %in efficiency and wouldn't help numerical issues at transition. Also
    %tried various ways for damping velocities to minimize oscillations,
    %some helped but might not fix transition issues and at that point
    %might as well just take this approach of setting u_gb = u_m.
    %best option just seems to be setting percolation thresholds negative,
    %so as to not deal with stokes regime or transition.

    %[var1=dP_dz, var2=dphi_dz, var3=du_mg_dz]
    const_vec = zeros(3,1);

    %bulk values
    rho_mg = phi*rho_g + (1 - phi)*rho_m;
    u_mg = u_m;


    % eq1: gas mass conservation
    %-q_w = 
    %+ phi*u_gb*(A)*drho_g_dz
    %+ (A)*(1 - phi)*rho_m*u_m*dn_d_dz
    %+ rho_g*u_gb*(A)*dphi_dz
    %+ phi*rho_g*(A)*du_mg_dz

    const_vec(1) = -q_w;
    g_mass_con_coeff_vec = zeros(1,3);
    %var1=dP_dz
    g_mass_con_coeff_vec(1) = ...
        + phi*u_g_avg*(A)*drho_g_dP_coeff...
        + (A)*(1 - phi)*rho_m*u_m*dn_d_dP_coeff;
    %var2=dphi_dz
    g_mass_con_coeff_vec(2) = rho_g*u_g_avg*(A);
    %var3=du_mg_dz
    g_mass_con_coeff_vec(3) = phi*rho_g*(A);


    % eq2: melt mass conservation
    %0 =
    %- (A)*(1 - phi)*rho_m*u_m*dn_d_dz
    %- rho_m*u_m*(A)*dphi_dz
    %+ (1 - phi)*rho_m*(A)*du_mg_dz

    const_vec(2) = 0;
    m_mass_con_coeff_vec = zeros(1,3);
    %var1=dP_dz
    m_mass_con_coeff_vec(1) = -(A)*(1 - phi)*rho_m*u_m*dn_d_dP_coeff;
    %var2=dphi_dz
    m_mass_con_coeff_vec(2) = -rho_m*u_m*(A);
    %var3=du_mg_dz
    m_mass_con_coeff_vec(3) = (1 - phi)*rho_m*(A);


    % eq3: melt+gas momentum conservation
    %F_mw + (A)*rho_mg*g = ...
    %-(A)*dP_dz...
    %-(A)*rho_mg*u_mg*du_mg_dz;

    const_vec(3) = F_mw + (A)*rho_mg*g;
    mg_mom_con_coeff_vec = zeros(1,3);
    %var1=dP_dz
    mg_mom_con_coeff_vec(1) = -(A);
    %var2=dphi_dz
    mg_mom_con_coeff_vec(2) = 0;
    %var3=du_mg_dz
    mg_mom_con_coeff_vec(3) = -(A)*rho_mg*u_mg;


    % solve for derivatives
    Coeff_mat = [g_mass_con_coeff_vec;...
        m_mass_con_coeff_vec;...
        mg_mom_con_coeff_vec];

    warning('off','MATLAB:nearlySingularMatrix')
    full_dvar_dz_vec = Coeff_mat\const_vec;
    warning('on','MATLAB:nearlySingularMatrix')

    %[var1=dP_dz, var2=dphi_dz, var3=du_mg_dz]
    dP_m_dz = full_dvar_dz_vec(1);
    dphi_dz = full_dvar_dz_vec(2);
    du_mg_dz = full_dvar_dz_vec(3);

    du_g_dz = du_mg_dz;
    du_m_dz = du_mg_dz; 
end

%debugging
% if ~isreal(dP_m_dz) || ~isreal(dphi_dz)
%     1
% end
    
end