function [n_d, dn_d_dz_coeff] = solubility_fun(pstruct, P)
%calculates solubility, disolved gas, derivatives
%Josh Crozier 2019

satConst = pstruct.satConst;
n_v0 = pstruct.n_v0;

%water solubility (Henry's Law)
C_w = satConst*sqrt(P);

%always have some small amount of exsolved gas
if C_w < n_v0 - pstruct.n_g_permanent
    n_d = C_w;
    %dn_d_dz = satConst*dP_dz/(2*sqrt(P)); 
    dn_d_dz_coeff = satConst/(2*sqrt(P));
else
    %always have some small amount of exsolved gas
    n_d = n_v0 - pstruct.n_g_permanent;
    dn_d_dz_coeff = 0;
end
end