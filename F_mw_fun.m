function [F_mw] = F_mw_fun(pstruct,...
    mu_m,...
    u_m,...
    phi,...
    A)

%force of melt on conduit walls   
%josh crozier 2019

r_c = sqrt(A/pi);
%assuming poiseulle magma flow
%not accounting for effect of bubbles or fractures on viscosity
F_mw = 8*mu_m*u_m/r_c^2;
%F_mw = 8*mu_m*u_m*((1-phi)^(5/3))/r_c^2;
                        
end