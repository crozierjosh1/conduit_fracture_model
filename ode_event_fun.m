function [value, isterminal, direction] = ode_event_fun(z,var_vec,pstruct)
%events/stop conditions for ode solving
%Josh Crozier 2019

P = var_vec(1);
phi = var_vec(2);
u_gb = var_vec(3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%porosity
value(1) = phi <= 0.9;
isterminal(1) = 1;
direction(1) = 0;
% disp('phi too high')

value(2) = phi >= 0;
isterminal(2) = 1;
direction(2) = 0;
% disp('phi too low')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%pressure
value(3) = P <= pstruct.P0;
isterminal(3) = 1;
direction(3) = 0;
% disp('P too high')

value(4) = P >= pstruct.Patm;
isterminal(4) = 1;
direction(4) = 0;
% disp('P too low')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% complex or NaN
value(5) = isreal(P) && isreal(phi) && isreal(u_gb);
isterminal(5) = 1;
direction(5) = 0;
% disp('complex')

value(6) = isfinite(P) && isfinite(phi) && isfinite(u_gb);
isterminal(6) = 1;
direction(6) = 0;
% disp('non-finite')

value = double(value);

end