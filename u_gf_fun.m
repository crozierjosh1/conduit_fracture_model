function [u_gf, u_gb] = u_gf_fun(pstruct,...
    A,...
    A_f,...
    phi,...
    rho_gb,...
    rho_gf,...
    q_g,...
    q_gf_frac,...
    u_m)
%calculates gas velocities from conseration of mass
%Josh Crozier 2019


%needed to avoid singularity
if A_f == 0
    u_gf = u_m;
    u_gb = q_g/(rho_gb*(A - A_f)*phi);
    
else
    q_gf = q_g*q_gf_frac;
    q_gb = q_g - q_gf;
    
    u_gf = q_gf/(rho_gf*A_f);
    u_gb = q_gb/(rho_gb*(A - A_f)*phi);
end

end