%{
sgrid.var_val{4} = sgrid.var_val{4}/1E6;
sgrid.var_val{3} = sgrid.var_val{3}*100;
sgrid.var_text{1} = 'conduit flare\newlineradius (m)';
sgrid.var_text{2} = 'conduit flare\newlinedepth (m)';
sgrid.var_text{3} = 'initial\newlineH_2O (wt.%)';
sgrid.var_text{4} = 'base\newlinepressure (MPa)';
sgrid.var_text{5} = 'total mass\newlineflux (kg/s)';
plot_grid_search_slices(log10(sgrid.w_fmax_grid), sgrid.d_f_grid,...
    [2,2,2,2,2], sgrid.var_text, sgrid.var_val,...
    'log_{10} max fracture width (m)', 'fracture depth (m)')
%}
%{
plot_grid_search_slices(sgrid.u_gfend_cell, sgrid.w_fend_cell,...
    [], sgrid.var_text, sgrid.var_val,...
    'log_{10} vent gas velocity in fractures (m/s)', 'log_{10} vent fracture width (m)',...
    1,1,sgrid.var1_ind_cell,sgrid.var2_ind_cell,sgrid.var_default)
plot_grid_search_slices(sgrid.d_f_cell, sgrid.phimax_cell,...
    [], sgrid.var_text, sgrid.var_val,...
    'fracture depth (m)', 'max porosity',...
    0,0,sgrid.var1_ind_cell,sgrid.var2_ind_cell,sgrid.var_default)
plot_grid_search_slices(sgrid.d_f_cell, sgrid.phimax_cell,...
    [], sgrid.var_text, sgrid.var_val,...
    'fracture depth (m)', 'max porosity',...
    0,0,sgrid.var1_ind_cell,sgrid.var2_ind_cell,sgrid.var_default)
%}

function plot_grid_search_slices(grid1,grid2,sliceinds,gridaxes,...
    gridaxesvalues,dataname1,dataname2,logscale1,logscale2,...
    var1_ind_cell,var2_ind_cell,var_default)
%plot slices of search grid
%josh crozier 2020


figure('Name', 'Grid Search')
ax = cell(length(gridaxes));
contour_plots = cell(length(gridaxes));
contourlevels1 = 20;
contourlevels2 = 20;
xscale = 0.65;
yscale = 0.65;
grid1_minmax = [inf,-inf];
if ~isempty(grid2)
    grid2_minmax = [inf,-inf];
end
if ismatrix(sliceinds)
    sliceinds = num2cell(sliceinds);
end
for i = 1:length(gridaxes)
    for j = 1:length(gridaxes)
        if i > j
            %% grid 1
            ax{i,j} = subplot('Position',[((j-1)*1/length(gridaxes))*0.68+0.16,...
                ((length(gridaxes)-i)*1/length(gridaxes))*0.76+0.13,...
                1/length(gridaxes)*xscale, 1/length(gridaxes)*yscale]); hold on
            
            if ~isempty(sliceinds)
                temp_sliceinds = sliceinds;
                temp_sliceinds{i} = 1:length(gridaxesvalues{i});
                temp_sliceinds{j} = 1:length(gridaxesvalues{j});
                temp_grid = squeeze(grid1(temp_sliceinds{:})).';
            else
                sliceind = find(cell2mat(var1_ind_cell) == j & ...
                    cell2mat(var2_ind_cell) == i);
                temp_grid = grid1{sliceind}';
            end
            if logscale1
                temp_grid = log10(temp_grid);
            end
            grid1_minmax(1) = min([min(temp_grid(:)), grid1_minmax(1)]);
            grid1_minmax(2) = max([max(temp_grid(:)), grid1_minmax(1)]);
            
%             p = pcolor(gridaxesvalues{j},gridaxesvalues{i},temp_grid);
%             p.FaceColor = 'interp';
%             p.EdgeColor = 'none';
            [M,contour_plots{i,j}] = contourf(gridaxesvalues{j},gridaxesvalues{i},temp_grid);
            contour_plots{i,j}.LineColor = 'k';
            xtickangle(90)

%             [M,contour_plots{i,j}] = contourf(gridaxesvalues{j},gridaxesvalues{i},temp_grid);
%             contour_plots{i,j}.LineColor = 'none';
            [xgrid,ygrid] = meshgrid(gridaxesvalues{j},gridaxesvalues{i});
            scatter(xgrid(:),ygrid(:),1,'r','.')
            if ~isempty(sliceinds)
                xline(gridaxesvalues{j}(sliceinds{j}),'r--','LineWidth',1.0);
                yline(gridaxesvalues{i}(sliceinds{i}),'r--','LineWidth',1.0);
            else
                xline(var_default{j},'r--','LineWidth',1.0)
                yline(var_default{i},'r--','LineWidth',1.0)
            end
            
            ylim([min(gridaxesvalues{i}),max(gridaxesvalues{i})])
            xlim([min(gridaxesvalues{j}),max(gridaxesvalues{j})])
            set(ax{i,j},'XGrid','on')
            set(ax{i,j},'YGrid','on')
            set(ax{i,j},'YDir','normal')
            set(ax{i,j},'layer','top')
%             shading(ax{i,j},'interp')
%             colormap(ax{i,j},flipud(cmocean('algae')))
            colormap(ax{i,j},flipud(cmocean('matter')))
            axis square
            box on
            
            if max(gridaxesvalues{i})/min(gridaxesvalues{i}) > 10
                set(ax{i,j},'Yscale','Log')
            end
            if max(gridaxesvalues{j})/min(gridaxesvalues{j}) > 10
                set(ax{i,j},'Xscale','Log')
            end
            
            set(ax{i,j},'XTick',gridaxesvalues{j})
            set(ax{i,j},'YTick',gridaxesvalues{i})
            
            if max(gridaxesvalues{j})<1E4 && max(gridaxesvalues{j})>1
                ticklabel = cellstr(num2str(gridaxesvalues{j}(:),'%.0f'));
                for kk = 2:2:length(ticklabel)
                    ticklabel{kk} = ' ';
                end
                set(ax{i,j},'XTickLabel',ticklabel)
            else
                ticklabel = cellstr(num2str(gridaxesvalues{j}(:),'%.2g'));
                for kk = 2:2:length(ticklabel)
                    ticklabel{kk} = ' ';
                end
                set(ax{i,j},'XTickLabel',ticklabel)
            end
            if max(gridaxesvalues{i})<1E4 && max(gridaxesvalues{i})>1
                ticklabel = cellstr(num2str(gridaxesvalues{i}(:),'%.0f'));
                for kk = 2:2:length(ticklabel)
                    ticklabel{kk} = ' ';
                end
                set(ax{i,j},'YTickLabel',ticklabel)
            else
                ticklabel = cellstr(num2str(gridaxesvalues{i}(:),'%.2g'));
                if length(ticklabel) > 5
                    for kk = 2:2:length(ticklabel)
                        ticklabel{kk} = ' ';
                    end
                end
                set(ax{i,j},'YTickLabel',ticklabel)
            end
            
            if i == length(gridaxes)
                xlabel(strrep(gridaxes{j},'_',''))
            else
                xticklabels([])
            end
            if j == 1
                ylabel(strrep(gridaxes{i},'_',''))
            else
                yticklabels([])
            end

        elseif i == j
            %% Center
            %{
            yyaxis('left')
            plot(gridaxesvalues{i},min_per_value{i},'-b','LineWidth',2)
            plot(gridaxesvalues{i},min_per_value{i},'.b','LineWidth',2)
            plot([min_misfti_param_value(j),min_misfti_param_value(j)],[-1E12,1E12],'k:','LineWidth',3)
            ylim([min_min_per_value,max_min_per_value])
            xlim([min(gridaxesvalues{i}),max(gridaxesvalues{i})])
            yyaxis('right')
            plot(gridaxesvalues{i},pdf_per_value{i},'-r','LineWidth',2)
            plot(gridaxesvalues{i},pdf_per_value{i},'.r','LineWidth',2)
            plot([min_misfti_param_value(j),min_misfti_param_value(j)],[-1E12,1E12],'k:','LineWidth',3)
            ylim([min_pdf_per_value,max_pdf_per_value])
            xlim([min(gridaxesvalues{i}),max(gridaxesvalues{i})])
            set(ax{i,j},'layer','top')
            %}

        elseif j > i
            if ~isempty(grid2)
                %% grid 2
                ax{i,j} = subplot('Position',[((j-1)*1/length(gridaxes))*0.68+0.16, ...
                    ((length(gridaxes)-i)*1/length(gridaxes))*0.76+0.13,...
                    1/length(gridaxes)*xscale, 1/length(gridaxes)*yscale], ...
                    'YAxisLocation','Right','XAxisLocation','top'); hold on
                if ~isempty(sliceinds)
                    temp_sliceinds = sliceinds;
                    temp_sliceinds{i} = 1:length(gridaxesvalues{i});
                    temp_sliceinds{j} = 1:length(gridaxesvalues{j});
                    temp_grid = squeeze(grid2(temp_sliceinds{:}));
                else
                    sliceind = find(cell2mat(var1_ind_cell) == i & ...
                        cell2mat(var2_ind_cell) == j);
                    temp_grid = grid2{sliceind};
                end
                if logscale2
                    temp_grid = log10(temp_grid);
                end
                grid2_minmax(1) = min([min(temp_grid(:)), grid2_minmax(1)]);
                grid2_minmax(2) = max([max(temp_grid(:)), grid2_minmax(1)]);

%                 p = pcolor(gridaxesvalues{j},gridaxesvalues{i},temp_grid);
%                 p.FaceColor = 'interp';
%                 p.EdgeColor = 'none';
                [M,contour_plots{i,j}] = contourf(gridaxesvalues{j},gridaxesvalues{i},temp_grid);
                contour_plots{i,j}.LineColor = 'k';
                xtickangle(90)

    %             [M,contour_plots{i,j}] = contourf(gridaxesvalues{j},gridaxesvalues{i},temp_grid);
    %             contour_plots{i,j}.LineColor = 'none';
                [xgrid,ygrid] = meshgrid(gridaxesvalues{j},gridaxesvalues{i});
                scatter(xgrid(:),ygrid(:),1,'r','.')
                if ~isempty(sliceinds)
                    xline(gridaxesvalues{j}(sliceinds{j}),'r--','LineWidth',1.0);
                    yline(gridaxesvalues{i}(sliceinds{i}),'r--','LineWidth',1.0);
                else
                    xline(var_default{j},'r--','LineWidth',1.0);
                    yline(var_default{i},'r--','LineWidth',1.0);
                end

                ylim([min(gridaxesvalues{i}),max(gridaxesvalues{i})])
                xlim([min(gridaxesvalues{j}),max(gridaxesvalues{j})])
                set(ax{i,j},'XGrid','on')
                set(ax{i,j},'YGrid','on')
                set(ax{i,j},'YDir','normal')
                set(ax{i,j},'layer','top')
    %             shading(ax{i,j},'interp')
%                 colormap(ax{i,j},flipud(cmocean('turbid')))
                colormap(ax{i,j},flipud(cmocean('dense')))
                axis square
                box on
                
                if max(gridaxesvalues{i})/min(gridaxesvalues{i}) > 10
                    set(ax{i,j},'Yscale','Log')
                end
                if max(gridaxesvalues{j})/min(gridaxesvalues{j}) > 10
                    set(ax{i,j},'Xscale','Log')
                end
                
                set(ax{i,j},'XTick',gridaxesvalues{j})
                set(ax{i,j},'YTick',gridaxesvalues{i})
            
                if max(gridaxesvalues{j})<1E4 && max(gridaxesvalues{j})>1
                    ticklabel = cellstr(num2str(gridaxesvalues{j}(:),'%.0f'));
                    for kk = 2:2:length(ticklabel)
                        ticklabel{kk} = ' ';
                    end
                    set(ax{i,j},'XTickLabel',ticklabel)
                else
                    ticklabel = cellstr(num2str(gridaxesvalues{j}(:),'%.2g'));
                    for kk = 2:2:length(ticklabel)
                        ticklabel{kk} = ' ';
                    end
                    set(ax{i,j},'XTickLabel',ticklabel)
                end
               
                if max(gridaxesvalues{i})<1E4 && max(gridaxesvalues{i})>1
                    ticklabel = cellstr(num2str(gridaxesvalues{i}(:),'%.0f'));
                    for kk = 2:2:length(ticklabel)
                        ticklabel{kk} = ' ';
                    end
                    set(ax{i,j},'YTickLabel',ticklabel)
                else
                    ticklabel = cellstr(num2str(gridaxesvalues{i}(:),'%.2g'));
                    for kk = 2:2:length(ticklabel)
                        ticklabel{kk} = ' ';
                    end
                    set(ax{i,j},'YTickLabel',ticklabel)
                end
                
                if i == 1
                    xlabel(strrep(gridaxes{j},'_',''))
                else
                    xticklabels([])
                end
                if j == length(gridaxes)
                    ylabel(strrep(gridaxes{i},'_',''))
                else
                    yticklabels([])
                end
            end
        end %end if i < j
        
    end %end loop j
end %end loop i


for i = 1:length(gridaxes)
    for j = 1:length(gridaxes)
        if i > j
            %% grid 1
            caxis(ax{i,j},grid1_minmax)
            contour_plots{i,j}.LevelList = linspace(grid1_minmax(1),grid1_minmax(2),...
                contourlevels1);
        elseif i < j
            if ~isempty(grid2)
                %% grid 2
                caxis(ax{i,j},grid2_minmax)
                contour_plots{i,j}.LevelList = linspace(grid2_minmax(1),grid2_minmax(2),...
                    contourlevels2);
            end
        end
        if i == length(gridaxes) && j == 1
            %
            pos = ax{i,j}.Position;
            c1 = colorbar(ax{i,j},'West');
%             c1.Position = [pos(1), pos(2)-4*pos(2)/10, pos(3)*(length(gridaxes)-1), pos(4)/20];
            c1.Position = [pos(1)-3.5*pos(3)/10, pos(2), pos(3)/20, pos(4)*(length(gridaxes)-1)];
            c1.TickDirection = 'out';
            c1.FontSize = 10;
            ylabel(c1,dataname1)
        end
        if i == length(gridaxes)-1 && j == length(gridaxes)
            %
            pos = ax{i,j}.Position;
            c2 = colorbar(ax{i,j},'East');
            c2.Position = [pos(1)+13.5*pos(3)/10, pos(2), pos(3)/20, pos(4)*(length(gridaxes)-1)];
            c2.TickDirection = 'out';
            c2.FontSize = 10;
            ylabel(c2,dataname2)
        end
    end
end
            
end %end function

