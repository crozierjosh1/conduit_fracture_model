%plot search grid

figure('Name','searchgrid')
r_c_grid = sgrid.r_c_grid;
phiend_grid = sgrid.phiend_grid;
phimax_grid = sgrid.phimax_grid;
u_gfend_grid = sgrid.u_gfend_grid;
u_gfmax_grid = sgrid.u_gfmax_grid;
r_c_grid(r_c_grid == 0) = NaN;
phiend_grid(phiend_grid == 0) = NaN;
phimax_grid(phimax_grid == 0) = NaN;
u_gfend_grid(u_gfend_grid == 0) = NaN;
u_gfmax_grid(u_gfmax_grid == 0) = NaN;
[d_fL_grid, w_f_grid] = meshgrid(sgrid.var_val{2},sgrid.var_val{1}); 
d_fL_lin_tot = d_fL_grid(:);
w_f_lin_tot = w_f_grid(:);
r_c_lin_tot = r_c_grid(:);
phiend_lin_tot = phiend_grid(:);
phimax_lin_tot = phimax_grid(:);
u_gfend_lin_tot = u_gfend_grid(:);
u_gfmax_lin_tot = u_gfmax_grid(:);
d_fL_lin = d_fL_lin_tot(~isnan(phimax_lin_tot));
w_f_lin = w_f_lin_tot(~isnan(phimax_lin_tot));
r_c_lin = r_c_lin_tot(~isnan(phimax_lin_tot));
phiend_lin = phiend_lin_tot(~isnan(phiend_lin_tot));
phimax_lin = phimax_lin_tot(~isnan(phimax_lin_tot));
u_gfend_lin = u_gfend_lin_tot(~isnan(u_gfend_lin_tot));
u_gfmax_lin = u_gfmax_lin_tot(~isnan(u_gfmax_lin_tot));
d_fL_lin_nan = d_fL_lin_tot(isnan(phimax_lin_tot));
w_f_lin_nan = w_f_lin_tot(isnan(phimax_lin_tot));
r_c_lin_nan = r_c_lin_tot(isnan(phimax_lin_tot));

txtup = 1.25;
txtright = 20;

ax1 = subplot(2,2,1);
% ph = pcolor(sgrid.var_val{2},sgrid.var_val{1},log10(P0_grid));
% ph.FaceColor = 'interp';
% ph.EdgeColor = 'none';
clevels = linspace(0,1,20);
[~,ch] = contourf(sgrid.var_val{2},sgrid.var_val{1},phimax_grid,clevels);
ch.LineColor = 'w';
ch.LineStyle = 'none';
hold on
scatter(d_fL_lin, w_f_lin, 30, phimax_lin,...
    'filled','MarkerEdgeColor','w')
text(d_fL_lin+txtright, w_f_lin*txtup, num2str(phimax_lin,'%.2f'))
scatter(d_fL_lin_nan, w_f_lin_nan, 30, 'k')
set(gca,'yscale','log')
set(gca,'xscale','linear')
xlabel('Fracture Depth (m)')
ylabel('Fracture Width (m)')
title(strcat('N_f= ',num2str(sgrid.pstruct.N_f),...
    ', L_c=',num2str(sgrid.pstruct.L),'m',...
    ', P_0=',num2str(sgrid.pstruct.P0*1E-6),'MPa',...
    ', q=',num2str(sgrid.pstruct.q),'kg/s','\newline Max Porosity'))
cbar = colorbar;
ylabel(cbar, 'max porosity')
% colormap(cmocean('haline'))
colormap(ax1,'cool');
grid minor
grid on
ax = gca;
ax.Layer = 'top';
set(gca,'GridAlpha',0.4)
set(gca,'GridColor',[0.8,0.8,0.8])
set(gca,'MinorGridAlpha',0.4)
set(gca,'MinorGridColor',[0.8,0.8,0.8])
caxis([0,1])
ylim([1E-3,3E0])
xlim([100,1900])

ax2 = subplot(2,2,2);
% ph = pcolor(sgrid.var_val{2},sgrid.var_val{1},log10(P0_grid));
% ph.FaceColor = 'interp';
% ph.EdgeColor = 'none';
clevels = linspace(0,1,20);
[~,ch] = contourf(sgrid.var_val{2},sgrid.var_val{1},phiend_grid,clevels);
ch.LineColor = 'w';
ch.LineStyle = 'none';
hold on
scatter(d_fL_lin, w_f_lin, 30, phiend_lin,...
    'filled','MarkerEdgeColor','w')
text(d_fL_lin+txtright, w_f_lin*txtup, num2str(phiend_lin,'%.2f'))
scatter(d_fL_lin_nan, w_f_lin_nan, 30, 'k')
set(gca,'yscale','log')
set(gca,'xscale','linear')
xlabel('Fracture Depth (m)')
ylabel('Fracture Width (m)')
title('\newline Final Porosity')
cbar = colorbar;
ylabel(cbar, 'final porosity')
% colormap(cmocean('haline'))
colormap(ax2,'cool');
grid minor
grid on
ax = gca;
ax.Layer = 'top';
set(gca,'GridAlpha',0.4)
set(gca,'GridColor',[0.8,0.8,0.8])
set(gca,'MinorGridAlpha',0.4)
set(gca,'MinorGridColor',[0.8,0.8,0.8])
caxis([0,1])
ylim([1E-3,3E0])
xlim([100,1900])

ax3 = subplot(2,2,3);
% ph = pcolor(sgrid.var_val{2},sgrid.var_val{1},log10(P0_grid));
% ph.FaceColor = 'interp';
% ph.EdgeColor = 'none';
clevels = linspace(log10(min(u_gfend_lin)),log10(max(u_gfmax_lin)),20);
[~,ch] = contourf(sgrid.var_val{2},sgrid.var_val{1},log10(u_gfmax_grid),clevels);
ch.LineColor = 'w';
ch.LineStyle = 'none';
hold on
scatter(d_fL_lin, w_f_lin, 30, log10(u_gfmax_lin),...
    'filled','MarkerEdgeColor','w')
text(d_fL_lin+txtright, w_f_lin*txtup, num2str(u_gfmax_lin,'%.1e'))
scatter(d_fL_lin_nan, w_f_lin_nan, 30, 'k')
set(gca,'yscale','log')
set(gca,'xscale','linear')
xlabel('Fracture Depth (m)')
ylabel('Fracture Width (m)')
title('Max Gas Velocity in Fractures')
cbar = colorbar;
ylabel(cbar, 'log10 max gas velocity (m/s)')
% colormap(cmocean('haline'))
colormap(ax3,'parula');
grid minor
grid on
ax = gca;
ax.Layer = 'top';
set(gca,'GridAlpha',0.4)
set(gca,'GridColor',[0.8,0.8,0.8])
set(gca,'MinorGridAlpha',0.4)
set(gca,'MinorGridColor',[0.8,0.8,0.8])
caxis([log10(min(u_gfend_lin)),log10(max(u_gfmax_lin))])
ylim([1E-3,3E0])
xlim([100,1900])

ax4 = subplot(2,2,4);
% ph = pcolor(sgrid.var_val{2},sgrid.var_val{1},log10(P0_grid));
% ph.FaceColor = 'interp';
% ph.EdgeColor = 'none';
clevels = linspace(log10(min(u_gfend_lin)),log10(max(u_gfmax_lin)),20);
[~,ch] = contourf(sgrid.var_val{2},sgrid.var_val{1},log10(u_gfend_grid),clevels);
ch.LineColor = 'w';
ch.LineStyle = 'none';
hold on
scatter(d_fL_lin, w_f_lin, 30, log10(u_gfend_lin),...
    'filled','MarkerEdgeColor','w')
text(d_fL_lin+txtright, w_f_lin*txtup, num2str(u_gfend_lin,'%.1e'))
scatter(d_fL_lin_nan, w_f_lin_nan, 30, 'k')
set(gca,'yscale','log')
set(gca,'xscale','linear')
xlabel('Fracture Depth (m)')
ylabel('Fracture Width (m)')
title('Final Gas Velocity in Fractures')
cbar = colorbar;
ylabel(cbar, 'log10 final gas velocity (m/s)')
% colormap(cmocean('haline'))
colormap(ax4,'parula');
grid minor
grid on
ax = gca;
ax.Layer = 'top';
set(gca,'GridAlpha',0.4)
set(gca,'GridColor',[0.8,0.8,0.8])
set(gca,'MinorGridAlpha',0.4)
set(gca,'MinorGridColor',[0.8,0.8,0.8])
caxis([log10(min(u_gfend_lin)),log10(max(u_gfmax_lin))])
ylim([1E-3,3E0])
xlim([100,1900])

%{
subplot(1,3,3)
% ph = pcolor(sgrid.var_val{2},sgrid.var_val{1},log10(P0_grid));
% ph.FaceColor = 'interp';
% ph.EdgeColor = 'none';
clevels = linspace(min(r_c_lin),max(r_c_lin),20);
[~,ch] = contourf(sgrid.var_val{2},sgrid.var_val{1},r_c_grid,clevels);
ch.LineColor = 'w';
ch.LineStyle = 'none';
hold on
scatter(d_fL_lin, w_f_lin, 30, r_c_lin,...
    'filled','MarkerEdgeColor','w')
scatter(d_fL_lin_nan, w_f_lin_nan, 30, 'k')
set(gca,'yscale','log')
set(gca,'xscale','linear')
xlabel('Fracture Depth (m)')
ylabel('Fracture Width (m)')
title(strcat('N_f= ',num2str(sgrid.pstruct.N_f),...
    ', L_c=',num2str(sgrid.pstruct.L),'m',...
    ', R_c=',num2str(sgrid.pstruct.r_c),'m',...
    ', q=',num2str(sgrid.pstruct.q),'kg/s'))
cbar = colorbar;
ylabel(cbar, 'conduit radius')
% colormap(cmocean('haline'))
colormap('autumn');
grid minor
grid on
ax = gca;
ax.Layer = 'top';
set(gca,'GridAlpha',0.4)
set(gca,'GridColor',[0.8,0.8,0.8])
set(gca,'MinorGridAlpha',0.4)
set(gca,'MinorGridColor',[0.8,0.8,0.8])
%}

