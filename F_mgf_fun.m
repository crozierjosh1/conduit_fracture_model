function [F_mgf] = F_mgf_fun(pstruct,...
    w_f,...
    rho_gf,...
    u_gf,...
    A_f,...
    u_m,...
    r_c)

%force of melt on gas in fractures   
%josh crozier 2019

%necessary to prevent singularity
if w_f == 0 || (u_gf - u_m) == 0
    F_mgf = 0;
    
else
    mu_g = pstruct.mu_g;
    highRe_Fanningfrictionfactor = pstruct.highRe_Fanningfrictionfactor;

    %assume fractures span conduit, so average length is
    L_f = pi/2*r_c;

    %laminar pipe Fanning friction factor
    f_F0pipe = 16*mu_g/(rho_gf*abs(u_gf - u_m)*w_f);

    %laminar-turbulent pipe Fanning friction factor
    f_Fpipe = f_F0pipe + highRe_Fanningfrictionfactor;

    %correction for fracture geometry
    f_F = 12*(w_f + L_f)/w_f*f_Fpipe;

    %confined turbulent flow
    F_mgf = A_f*rho_gf*(u_gf - u_m)*abs(u_gf - u_m)*f_F;
end
                        
end