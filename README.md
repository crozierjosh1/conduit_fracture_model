This readme file was generated on 2022-1-18 by Josh Crozier


GENERAL INFORMATION

1. Title of Dataset: Inversion results and codes corresponding to manuscript "Outgassing through fractures enables effusive eruption of silicic magma"

2. Author Information
	A. Principal Contact Information
		Name: Josh Crozier
		Institution: University of Oregon (now USGS California Volcano Observatory)
		Email: jcrozier@usgs.gov or crozierjosh1@gmail.com


3. Date of data collection (single date, range, approximate date): 
2008-01-01 to 2018-08-01

4. Geographic location of data collection: 
Cordon Caulle, Chile

5. Information about funding sources that supported the collection of the data: 
Cooperative institute for dynamic earth research (CIDER)


SHARING/ACCESS INFORMATION

1. Licenses/restrictions placed on the data:
Public Domain/CC-0

2. Links to publications that cite or use the data: 
Crozier, J., Tramontano, S., et al. Outgassing through fractures enables effusive eruption of silicic magma

3. Links to other publicly accessible locations of the data: 


4. Links/relationships to ancillary data sets: 
None

5. Was data derived from another source? 
yes, video data was obtained from the BBC

6. Recommended citation for this dataset: 
please cite the publication "Outgassing through fractures enables effusive eruption of silicic magma"


DATA & FILE OVERVIEW

1. File List: 

main_minphi.m is the main script that runs the conduit model
CordonCaulle_velo.m in the video_analysis folder is the main script for video analysis

Other files are helper functions and/or plotting resources


2. Relationship between files, if important: 
see file descriptions above

3. Additional related data collected that was not included in the current data package: 
None

4. Are there multiple versions of the dataset? 
no


METHODOLOGICAL INFORMATION

1. Description of methods used for collection/generation of data: 
see publication "Outgassing through fractures enables effusive eruption of silicic magma"

2. Methods for processing the data: 
see publication "Outgassing through fractures enables effusive eruption of silicic magma"

3. Instrument- or software-specific information needed to interpret the data: 
Matlab r2021, Matlab Image analysis toolbox (for video analysis), (optional) Matlab Parallel Computing Toolbox
(codes were tested on both a Windows 10 PC and linux based Talapas HPC https://hpcf.uoregon.edu/content/talapas)

4. Standards and calibration information, if appropriate: 
NA

5. Environmental/experimental conditions: 
NA

6. Describe any quality-assurance procedures performed on the data: 
see publication "Outgassing through fractures enables effusive eruption of silicic magma"

7. People involved with sample collection, processing, analysis and/or submission: 
Josh Crozier, Samantha Tramontano
