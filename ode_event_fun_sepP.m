function [value, isterminal, direction] = ode_event_fun_sepP(z,var_vec,pstruct)
%events/stop conditions for ode solving
%Josh Crozier 2019

P_m = var_vec(1); %Pressure (Pa)
phi = var_vec(2); %Porosity (gas volume fraction, not counting fractures)

if length(var_vec) >= 3 
    q_gf_rat = var_vec(3); %gas mass flux in fractures relative to bubble gas mass flux
end
if length(var_vec) >= 4 
    P_gf = var_vec(4); %gas mass flux in fractures relative to bubble gas mass flux
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%porosity
value(1) = phi <= 1;
isterminal(1) = 1;
direction(1) = 0;
% if ~value(1)
% 	disp('phi too high')
% end

value(2) = phi >= 0;
isterminal(2) = 1;
direction(2) = 0;
% if ~value(2)
% 	disp('phi too low')
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%magma pressure
value(3) = P_m <= pstruct.P0;
isterminal(3) = 1;
direction(3) = 0;
% if ~value(3)
% 	disp('P_m too high')
% end

value(4) = P_m > pstruct.Patm;
isterminal(4) = 1;
direction(4) = 0;
% if ~value(4)
% 	disp('P_m too low')
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% complex or NaN
value(5) = isreal(P_m) && isreal(phi);
isterminal(5) = 1;
direction(5) = 0;
% if ~value(5)
%     disp('complex')
% end

value(6) = isfinite(P_m) && isfinite(phi);
isterminal(6) = 1;
direction(6) = 0;
% if ~value(6)
%     disp('non-finite')
% end


if length(var_vec) >= 3
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %q_gf_rat
    value(7) = q_gf_rat >= 0;
    isterminal(7) = 1;
    direction(7) = 0;
%     if ~value(7)
%         disp('q_gf_rat too low')
%     end
    
    value(8) = isreal(q_gf_rat) ;
    isterminal(8) = 1;
    direction(8) = 0;
%     if ~value(8)
%         disp('complex')
%     end

    value(9) = isfinite(q_gf_rat);
    isterminal(9) = 1;
    direction(9) = 0;
%     if ~value(9)
%         disp('non-finite')
%     end
end
if length(var_vec) >= 4
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %gas fracture pressure
    value(10) = P_gf <= pstruct.P0;
    isterminal(10) = 1;
    direction(10) = 0;
%     if ~value(10)
%         disp('P_gf too high')
%     end

    value(11) = P_gf >= pstruct.Patm;
    isterminal(11) = 1;
    direction(11) = 0;
%     if ~value(11)
%         disp('P_gf too low')
%     end
    
    value(12) = isreal(P_gf);
    isterminal(12) = 1;
    direction(12) = 0;
%     if ~value(12)
%         disp('complex')
%     end

    value(13) = isfinite(P_gf);
    isterminal(13) = 1;
    direction(13) = 0;
%     if ~value(13)
%         disp('non-finite')
%     end
end

value = double(value);

end