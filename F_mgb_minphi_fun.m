function [F_mgb, k_1, k_2] = F_mgb_minphi_fun(pstruct,...
    phi_b,...
    phi_br,...
    mu_m,...
    u_gb,...
    u_m,...
    rho_gb,...
    A,...
    compaction,...
    phi_br_precompaction,...
    k_1_precompaction)

perm_b = pstruct.perm_b;
perm_n = pstruct.perm_n;
perm_phi_crit = pstruct.perm_phi_crit;

%'not using compaction'
% compaction = false;

%force of melt on gas in bubbles  
%josh crozier 2019

%!!!!!hardcoded for now!!!!!!!!!!!
%from gonnermann 2017
%percolation model
% b = 1.04; '!!!!seems wrong!!!!!'
% n = 3.21;
% phi_crit = 0.4;
%glass mountain
% perm_b = 10^(-8.892);
% perm_n = 2;
% perm_phi_crit = 0.74;
min_k_1 = 10*eps; %small value to prevent numerical issues

%necessary to prevent singularity
if phi_b == 0
    F_mgb = 0;
    k_1 = 0;
    k_2 = 0;
    
else
    N_b = pstruct.N_b;
    throatratio = pstruct.throatratio;
    tortuosityfactor = pstruct.tortuosityfactor;
    bubbleroughnessfactor = pstruct.bubbleroughnessfactor;
    phi_pL = pstruct.phi_pL;
    phi_pU = pstruct.phi_pU;
    mu_g = pstruct.mu_g;
    r_b_min = pstruct.r_b_min;
%     g = pstruct.g;

    %bubble radius
    r_b = (3*phi_br/(4*pi*N_b*(1 - phi_br)))^(1/3);
    %set min bubble radius to keep drag force from being way to big, and
    %also because at some point the assumption of constant BND probably
    %breaks down
    if r_b < r_b_min
       r_b = r_b_min;
    end

    %repeating scaling
    Fscale = A*phi_b*(1 - phi_b)*(u_gb - u_m);
    %Bscale = (A - A_f)*phi*g;

    if phi_br <= phi_pL
        %stokes drag
        F_mgb = ((3*mu_m)/(r_b^2))...
            *Fscale;
        
        k_1 = 0;
        k_2 = 0;
        
        %bubble bouyancy
        %F_B = Bscale;

    elseif phi_br > phi_pL && phi_br < phi_pU
        %transition
        t = (phi_br - phi_pL)/(phi_pU - phi_pL);
        
        %stokes drag
        D_St = (3*mu_m)/(r_b^2);
        
%         %bubble connection throat radius
%         r_t = throatratio*r_b;
%         %darcy permeability
%         %karmen-cozeny
%         k_1 = r_t^2/8*phi_br^tortuosityfactor; 
%         %inertial permeability (forcheimer law)
%         k_2 = r_t/bubbleroughnessfactor*phi_br^((1 + 3*tortuosityfactor)/2);
           
        %gonnermann 2017
        if ~compaction
            if phi_br > perm_phi_crit
                k_1 = perm_b*(phi_br/(1-phi_br))^(2/3)*(phi_br - perm_phi_crit)^perm_n;
            else
                k_1 = 0;
            end
        else
%             k_1_precompaction = b*(phi_br_precompaction/(1-phi_br_precompaction))^(2/3)*(phi_br_precompaction - phi_crit)^n;
%             const = (k_1_precompaction/((phi_br_precompaction-1)^(-n - 2/3)*phi_br_precompaction^n));
%             k_1 = (const*(phi_br-1)^(-n - 2/3)*phi_br^n);
            dk_1_dphi = (perm_n + 2/3)*k_1_precompaction/phi_br + ...
                2/3*k_1_precompaction/(1 - phi_br);
            dphi = phi_br - phi_br_precompaction;
            k_1 = k_1_precompaction + dk_1_dphi * dphi;
        end
        if k_1 < min_k_1
            k_1 = min_k_1; %small value to prevent numerical issues
        end
%         if k_1 < 10^-16
%             k_1 = 10^-16; %might be needed for numerical stability
%         end
        k_2 = 10^(1.353*log10(k_1) + 8.175);       
        
        %forcheimer permeability (darcy + inertial)
        D_DF = (mu_g)/(k_1) + (rho_gb)/(k_2)*abs(u_gb - u_m);
        
        %transition drag
        F_mgb = (D_St + 1/2*(D_St - D_DF)*(cos(pi*t) - 1))...
            *Fscale;
        
%         %partial bubble bouyancy
%         F_B = 1 + 1/2*(1 - throatratio^2)*(cos(pi*t) - 1)...
%             *Bscale;

    else
%         %bubble connection throat radius
%         r_t = throatratio*r_b;     
%         %darcy permeability
%         %karmen-cozeny
%         k_1_old = r_t^2/8*phi_br^tortuosityfactor; 
%         %inertial permeability (forcheimer law)
%         k_2_old = r_t/bubbleroughnessfactor*phi_br^((1 + 3*tortuosityfactor)/2);
           
        %gonnermann 2017
        if ~compaction
            if phi_br > perm_phi_crit
                k_1 = perm_b*(phi_br/(1-phi_br))^(2/3)*(phi_br - perm_phi_crit)^perm_n;
            else
                k_1 = 0;
            end
        else
%             k_1_precompaction = b*(phi_br_precompaction/(1-phi_br_precompaction))^(2/3)*(phi_br_precompaction - phi_crit)^n;
%             const = (k_1_precompaction/((phi_br_precompaction-1)^(-n - 2/3)*phi_br_precompaction^n));
%             k_1 = (const*(phi_br-1)^(-n - 2/3)*phi_br^n);
            dk_1_dphi = (perm_n + 2/3)*k_1_precompaction/phi_br + ...
                2/3*k_1_precompaction/(1 - phi_br);
            dphi = phi_br - phi_br_precompaction;
            k_1 = k_1_precompaction + dk_1_dphi * dphi;
        end
        if k_1 < min_k_1
            k_1 = min_k_1; %small value to prevent numerical issues
        end
%         if k_1 < 10^-16
%             k_1 = 10^-16; %might be needed for numerical stability
%         end
        k_2 = 10^(1.353*log10(k_1) + 8.175); 
        
        
        %forcheimer permeability (darcy + inertial)
        F_mgb = ((mu_g)/(k_1) + (rho_gb)/(k_2)*abs(u_gb - u_m))...
            *Fscale;
        
                
%         %debug pause
%         if phi > 0.75
%             disp(k_1)
%             disp(k_2)
%             'debug print f_mgb_fun'
%         end
        
        
%         %partial bubble bouyancy
%         F_B = throatratio^2*Bscale;
        
    end
end

%dubug
% if phi > phi_pL + 1/4*(phi_pU-phi_pL)
%     1;
% end
% if ~isreal(k_1)
%     'debug'
% end
                        
end