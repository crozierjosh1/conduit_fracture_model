%% Draft script for Cordon Caulle video analysis
%modified by josh crozier Jan 2022
%% Load the video:
M = VideoReader('CC-stitch.mp4');

%% Scale
'!!!need to input correct meters_per_pixel!!!'
meter_per_pixel = 0.25;

%% Figure out the window to examine:
figure()
frameA = M.readFrame;
M.CurrentTime = M.CurrentTime - 1/M.FrameRate;

rect = [100,100,500,500]; %[xmin ymin width height]
'manually cropping frames, uncomment next line to crop interactively'
% [~, rect] = imcrop(frameA); % this will provide the boundaries for the frame, to be used in the loop

%% plot settings
makeplots = true; %plot quivers and etc, will probably slow things down a lot
q_inc = 12; %quiver plot pixel increment (otherwise way too many vectors to see)
q_plot_max = 40; %max vel scale for quiver plot
v_plot_max = 100; %max vel scale for other plots
numbins_plot = 30; %number of histogram bins

%% data saving settings
filename = 'testrun3';
v_max = 50; %max velocity bin (m/s)
numbins = 200; %number of bins for velocity histograms
Vthresh = 5; %fixed threshold on min vertical vel to count (m/s)
qthresh = 0.8; %quantile threshold on min vertical vel to count
num_fbin = 60; %number of bins for amplitude spectra, recommend <100 to ensure enough points in each
num_Bbin = 100; %number of bins for brighness histograms

%% loop settings
%rect=[1128.5 700.5 218 82]; %from check position in image above
'should make dt a multiple of frame period'
dt = 1*(1/M.FrameRate); % in seconds. Frame-to-frame time difference for velocities. Can be larger or smaller than 'step'

step = dt*100; % in seconds. Time to skip between analyzed frames. To quicken the processing.
% Change to whatever you want, ideally multiple of dt or might cause issues.

%% make tables to store data in
xyedges = linspace(-v_max,v_max,2*numbins+1);
Bedges = linspace(0,255,num_Bbin);

numrows = ceil(M.Duration/step);
numcols = length(xyedges);
varTypes = cell(1,length(xyedges));
varTypes{1} = 'double';
varNames = cell(1,length(xyedges));
varNames{1} = 'time';
for ii = 1:length(xyedges)-1
    varTypes{ii+1} = 'double';
    varNames{ii+1} = strcat(num2str((xyedges(ii)+xyedges(ii+1))/2,'%.5f'));
end
Uhist_table = table('Size',[numrows,numcols],'VariableTypes',varTypes,'VariableNames',varNames);
Vhist_table = table('Size',[numrows,numcols],'VariableTypes',varTypes,'VariableNames',varNames);

numcols = length(Bedges);
varTypes = cell(1,length(Bedges));
varTypes{1} = 'double';
varNames = cell(1,length(Bedges));
varNames{1} = 'time';
for ii = 1:length(Bedges)-1
    varTypes{ii+1} = 'double';
    varNames{ii+1} = strcat(num2str((Bedges(ii)+Bedges(ii+1))/2,'%.5f'));
end
Bhist_table = table('Size',[numrows,numcols],'VariableTypes',varTypes,'VariableNames',varNames);

stats_table = table('Size',[numrows,1],'VariableTypes',{'double'},'VariableNames',{'time'});

%mean, med, std of velocities
% stats_table.U_mean = NaN(numrows,1);
% stats_table.V_mean = NaN(numrows,1);
% stats_table.Vmag_mean = NaN(numrows,1);
% 
% stats_table.U_med = NaN(numrows,1);
% stats_table.V_med = NaN(numrows,1);
% stats_table.Vmag_med = NaN(numrows,1);
% 
% stats_table.U_std = NaN(numrows,1);
% stats_table.V_std = NaN(numrows,1);
% stats_table.Vmag_std = NaN(numrows,1);

%mean, med, std of velocities above some fixed threshold
stats_table.U_mean_thresh = NaN(numrows,1);
stats_table.V_mean_thresh = NaN(numrows,1);
stats_table.Vmag_mean_thresh = NaN(numrows,1);

stats_table.U_med_thresh = NaN(numrows,1);
stats_table.V_med_thresh = NaN(numrows,1);
stats_table.Vmag_med_thresh = NaN(numrows,1);

stats_table.U_q75_thresh = NaN(numrows,1);
stats_table.V_q75_thresh = NaN(numrows,1);
stats_table.Vmag_q75_thresh = NaN(numrows,1);

stats_table.U_q85_thresh = NaN(numrows,1);
stats_table.V_q85_thresh = NaN(numrows,1);
stats_table.Vmag_q85_thresh = NaN(numrows,1);

stats_table.U_q95_thresh = NaN(numrows,1);
stats_table.V_q95_thresh = NaN(numrows,1);
stats_table.Vmag_q95_thresh = NaN(numrows,1);

stats_table.U_q99_thresh = NaN(numrows,1);
stats_table.V_q99_thresh = NaN(numrows,1);
stats_table.Vmag_q99_thresh = NaN(numrows,1);

stats_table.U_std_thresh = NaN(numrows,1);
stats_table.V_std_thresh = NaN(numrows,1);
stats_table.Vmag_std_thresh = NaN(numrows,1);

%mean, med, std of velocities above some threshold quantile
% stats_table.U_mean_qthresh = NaN(numrows,1);
% stats_table.V_mean_qthresh = NaN(numrows,1);
% stats_table.Vmag_mean_qthresh = NaN(numrows,1);
% 
% stats_table.U_med_qthresh = NaN(numrows,1);
% stats_table.V_med_qthresh = NaN(numrows,1);
% stats_table.Vmag_med_qthresh = NaN(numrows,1);
% 
% stats_table.U_std_qthresh = NaN(numrows,1);
% stats_table.V_std_qthresh = NaN(numrows,1);
% stats_table.Vmag_std_qthresh = NaN(numrows,1);

%quantiles of velocities
% stats_table.U_quant75 = NaN(numrows,1);
% stats_table.V_quant75 = NaN(numrows,1);
% stats_table.Vmag_quant75 = NaN(numrows,1);
% 
% stats_table.U_quant90 = NaN(numrows,1);
% stats_table.V_quant90 = NaN(numrows,1);
% stats_table.Vmag_quant90 = NaN(numrows,1);
% 
% stats_table.U_quant95 = NaN(numrows,1);
% stats_table.V_quant95 = NaN(numrows,1);
% stats_table.Vmag_quant95 = NaN(numrows,1);

%brightness
stats_table.B_mean = NaN(numrows,1);
stats_table.B_med = NaN(numrows,1);
stats_table.B_std = NaN(numrows,1);

stats_table.B_quant25 = NaN(numrows,1);
stats_table.B_quant75 = NaN(numrows,1);


%% main loop
ii=1;%loop counting variable
tic
while M.CurrentTime < M.Duration-dt
    
    if mod(ii,10) == 1
        disp([ii,numrows,M.CurrentTime,toc])
    end
        
    % read frames
    frameA = M.readFrame;
    M.CurrentTime = M.CurrentTime - 1/M.FrameRate;
    frameAcut = frameA(rect(2):rect(2)+rect(4),rect(1):rect(1)+rect(3));
%     frameAcut = imcrop(frameA, rect);
    M.CurrentTime = M.CurrentTime + dt;
    frameB = M.readFrame;
    M.CurrentTime = M.CurrentTime - 1/M.FrameRate;
    frameBcut = frameB(rect(2):rect(2)+rect(4),rect(1):rect(1)+rect(3));
%     frameBcut = imcrop(frameB, rect);
    
    % Analyze velocities
    UV = estimate_flow_hs (frameAcut, frameBcut, 'ba-brightness'   );  
    
    %scale to actual velocities
    UV = UV*meter_per_pixel/dt;
    
    %components
    U = squeeze(UV(:,:,1));
    V = squeeze(UV(:,:,2));
    Vmag = sqrt(U.^2 + V.^2);
    
    % histogram counts to output
    Uhist = histcounts(U(:), xyedges, 'Normalization','count');
    Vhist = histcounts(V(:), xyedges, 'Normalization','count');
    Uhist_table{ii,1} = M.CurrentTime;
    Uhist_table{ii,2:end} = Uhist;
    Vhist_table{ii,1} = M.CurrentTime;
    Vhist_table{ii,2:end} = Vhist; 
    
    %update stats table
    stats_table.time(ii) = M.CurrentTime;
    
    %mean, med, std of velocities
%     stats_table.U_mean(ii) = mean(U(:));
%     stats_table.V_mean(ii) = mean(V(:));
%     stats_table.Vmag_mean(ii) = mean(Vmag(:));
% 
%     stats_table.U_med(ii) = median(U(:));
%     stats_table.V_med(ii) = median(V(:));
%     stats_table.Vmag_med(ii) = median(Vmag(:));
% 
%     stats_table.U_std(ii) = std(U(:));
%     stats_table.V_std(ii) = std(V(:));
%     stats_table.Vmag_std(ii) = std(Vmag(:));

    %mean, med, std of velocities above some fixed threshold
    stats_table.U_mean_thresh(ii) = mean(U(V > Vthresh));
    stats_table.V_mean_thresh(ii) = mean(V(V > Vthresh));
    stats_table.Vmag_mean_thresh(ii) = mean(Vmag(V > Vthresh));

    stats_table.U_med_thresh(ii) = median(U(V > Vthresh));
    stats_table.V_med_thresh(ii) = median(V(V > Vthresh));
    stats_table.Vmag_med_thresh(ii) = median(Vmag(V > Vthresh));
    
    stats_table.U_q75_thresh(ii) = quantile(U(V > Vthresh),0.75);
    stats_table.V_q75_thresh(ii) = quantile(V(V > Vthresh),0.75);
    stats_table.Vmag_q75_thresh(ii) = quantile(Vmag(V > Vthresh),0.75);
    
    stats_table.U_q85_thresh(ii) = quantile(U(V > Vthresh),0.85);
    stats_table.V_q85_thresh(ii) = quantile(V(V > Vthresh),0.85);
    stats_table.Vmag_q85_thresh(ii) = quantile(Vmag(V > Vthresh),0.85);
    
    stats_table.U_q95_thresh(ii) = quantile(U(V > Vthresh),0.95);
    stats_table.V_q95_thresh(ii) = quantile(V(V > Vthresh),0.95);
    stats_table.Vmag_q95_thresh(ii) = quantile(Vmag(V > Vthresh),0.95);
    
    stats_table.U_q99_thresh(ii) = quantile(U(V > Vthresh),0.99);
    stats_table.V_q99_thresh(ii) = quantile(V(V > Vthresh),0.99);
    stats_table.Vmag_q99_thresh(ii) = quantile(Vmag(V > Vthresh),0.99);

    stats_table.U_std_thresh(ii) = std(U(V > Vthresh));
    stats_table.V_std_thresh(ii) = std(V(V > Vthresh));
    stats_table.Vmag_std_thresh(ii) = std(Vmag(V > Vthresh));

    %quantiles of velocities
%     stats_table.U_quant75(ii) = quantile(U(:),0.75);
%     stats_table.V_quant75(ii) = quantile(V(:),0.75);
%     stats_table.Vmag_quant75(ii) = quantile(Vmag(:),0.75);
% 
%     stats_table.U_quant90(ii) = quantile(U(:),0.9);
%     stats_table.V_quant90(ii) = quantile(V(:),0.9);
%     stats_table.Vmag_quant90(ii) = quantile(Vmag(:),0.9);
% 
%     stats_table.U_quant95(ii) = quantile(U(:),0.95);
%     stats_table.V_quant95(ii) = quantile(V(:),0.95);
%     stats_table.Vmag_quant95(ii) = quantile(Vmag(:),0.95);
    
    %mean, med, std of velocities above some threshold quantile
%     Vqthresh = quantile(V(:),qthresh);
%     stats_table.U_mean_qthresh(ii) = mean(U(V > Vqthresh));
%     stats_table.V_mean_qthresh(ii) = mean(V(V > Vqthresh));
%     stats_table.Vmag_mean_qthresh(ii) = mean(Vmag(V > Vqthresh));
% 
%     stats_table.U_med_qthresh(ii) = median(U(V > Vqthresh));
%     stats_table.V_med_qthresh(ii) = median(V(V > Vqthresh));
%     stats_table.Vmag_med_qthresh(ii) = median(Vmag(V > Vqthresh));
% 
%     stats_table.U_std_qthresh(ii) = std(U(V > Vqthresh));
%     stats_table.V_std_qthresh(ii) = std(V(V > Vqthresh));
%     stats_table.Vmag_std_qthresh(ii) = std(Vmag(V > Vqthresh));

  
    %% brightness    
    B = double(rgb2gray(frameAcut));
    
    Bhist = histcounts(B(:), Bedges, 'Normalization','count');
    Bhist_table{ii,1} = M.CurrentTime;
    Bhist_table{ii,2:end} = Bhist;
    
    stats_table.B_mean(ii) = mean(B(:));
    stats_table.B_med(ii) = median(B(:));
    stats_table.B_std(ii) = std(B(:));
    stats_table.B_quant25(ii) = quantile(B(:),0.25);
    stats_table.B_quant75(ii) = quantile(B(:),0.75);
    
    
    %% amplitude spectra
    %{
    %trim if odd frame dimensions
    if mod(size(B,1),2) == 1
        B = B(1:end-1,:);
        U = U(1:end-1,:);
        V = V(1:end-1,:);
    end
    if mod(size(B,2),2) == 1
        B = B(:,1:end-1);
        U = U(:,1:end-1);
        V = V(:,1:end-1);
    end
    
    if ii == 1
        fmax = 0.5;
        framesize = size(B);
        fmin = 1/max(framesize);
        fedges = linspace(fmin,fmax,num_fbin+1);
        varTypes = cell(1,length(fedges));
        varTypes{1} = 'double';
        varNames = cell(1,length(fedges));
        varNames{1} = 'time';
        for jj = 1:length(fedges)-1
            varTypes{jj+1} = 'double';
            varNames{jj+1} = strcat(num2str((fedges(jj)+fedges(jj+1))/2/meter_per_pixel,'%.5f'));
        end
        Uspec_table = table('Size',[numrows,length(fedges)],'VariableTypes',varTypes,'VariableNames',varNames);
        Vspec_table = table('Size',[numrows,length(fedges)],'VariableTypes',varTypes,'VariableNames',varNames);
        Bspec_table = table('Size',[numrows,length(fedges)],'VariableTypes',varTypes,'VariableNames',varNames);
    end
    
    Uspec_table{ii,1} = M.CurrentTime;
    Uspec_table{ii,2:end} = Aspec2d(U,fedges);
    Vspec_table{ii,1} = M.CurrentTime;
    Vspec_table{ii,2:end} = Aspec2d(V,fedges);
    Bspec_table{ii,1} = M.CurrentTime;
    Bspec_table{ii,2:end} = Aspec2d(B,fedges);
    %}
    
    
    if makeplots
        %% plot
        %frame plot
        if ii == 1
            figure()
            frame_ax = subplot('Position',[0.1,0.5,0.65,0.4]); hold on
            axis equal
            set(gca,'Ydir','reverse')
            xlim([1,size(Vmag,2)]), ylim([1,size(Vmag,1)])
            xlabel('x'), ylabel('y')
            grid on
        else
            delete(frame_plot)
        end
        frame_plot = image(frame_ax,1:size(Vmag,2),1:size(Vmag,1),frameBcut);

        
        %quiver plot
        if ii == 1
            x = (1:q_inc:size(U,2));%+rect(1);
            y = (1:q_inc:size(U,1));%+rect(2);
            [xg,yg] = meshgrid(x,y);
        else
            delete(q_plot)
        end
        Uplot = U(1:q_inc:end,1:q_inc:end);
        Vplot = V(1:q_inc:end,1:q_inc:end);
        q_plot = quiver(frame_ax,xg(VPlot>=Vthresh),yg(VPlot>=Vthresh),Uplot(VPlot>=Vthresh),Vplot(Vplot>=Vthresh),...
            q_inc/q_plot_max,'b');
        q_plot = quiver(frame_ax,xg(VPlot<Vthresh),yg(VPlot<Vthresh),Uplot(VPlot<Vthresh),Vplot(Vplot<Vthresh),...
            q_inc/q_plot_max,'r');
        
        %vertical and horizontal averages
        if ii == 1
            pos = plotboxpos(frame_ax);
            xavg_ax = subplot('Position',[pos(1),0.35,pos(3),0.09]); hold on
            xlabel('x'), ylabel('Velocity\newline(mean & max)')
            grid on
            xlim([1,size(Vmag,2)]), ylim([0,v_plot_max])
            
            yavg_ax = subplot('Position',[0.8,pos(2),0.09,pos(4)]); hold on
            xlabel('Velocity\newline(mean & max)'), ylabel('y')
            grid on
            xlim([0,v_plot_max]), ylim([1,size(Vmag,1)])
            set(gca,'Ydir','reverse')
        else
            delete([xavg_plot,xmax_plot])
            delete([yavg_plot,ymax_plot])
        end
        pos = plotboxpos(frame_ax);
        set(xavg_ax,'Position',[pos(1),0.35,pos(3),0.09])
        set(yavg_ax,'Position',[0.85,pos(2),0.09,pos(4)])
        xavg_plot = plot(xavg_ax,1:size(Vmag,2),mean(Vmag,1),'b');
        xmax_plot = plot(xavg_ax,1:size(Vmag,2),max(Vmag,[],1),'g');
        yavg_plot = plot(yavg_ax,mean(Vmag,2),1:size(Vmag,1),'b');
        ymax_plot = plot(yavg_ax,max(Vmag,[],2),1:size(Vmag,1),'g');
        
        %histograms
        if ii == 1
            Uhist_ax = subplot('Position',[0.1,0.1,0.25,0.19]);
            Vhist_ax = subplot('Position',[0.4,0.1,0.25,0.19]);
            Vmaghist_ax = subplot('Position',[0.7,0.1,0.2,0.19]);
            xyedges_plot = linspace(-v_plot_max,v_plot_max,2*numbins_plot+1);
            edges_plot = linspace(0,v_plot_max,numbins_plot+1);
        else
            delete([xhist_plot,yhist_plot,hist_plot])
        end
        xhist_plot = histogram(Uhist_ax, U(:), xyedges_plot, 'Normalization','probability');
        xlabel(Uhist_ax, 'X Velocity'), ylabel(Uhist_ax, 'probability')
        xlim(Uhist_ax, [-v_plot_max,v_plot_max]), ylim(Uhist_ax, [0,1])
        grid(Uhist_ax,'on')
        
        yhist_plot = histogram(Vhist_ax, V(:), xyedges_plot, 'Normalization','probability');
        xlabel(Vhist_ax, 'Y Velocity'), ylabel(Vhist_ax, 'probability')
        xlim(Vhist_ax, [-v_plot_max,v_plot_max]), ylim(Vhist_ax, [0,1])
        grid(Vhist_ax,'on')
        
        hist_plot = histogram(Vmaghist_ax, Vmag(:), edges_plot, 'Normalization','probability');
        xlabel(Vmaghist_ax, 'Velocity'), ylabel(Vmaghist_ax, 'probability')
        xlim(Vmaghist_ax, [0,v_plot_max]), ylim(Vmaghist_ax, [0,1])
        grid(Vmaghist_ax,'on')
        
    end
    
    
    % advance counter and video time marker
    if M.CurrentTime + step + dt > M.Duration
        break
    else
        ii = ii+1;
        M.CurrentTime = M.CurrentTime + step - dt;
    end
end

%% Export data
writetable(stats_table,strcat(filename,'_stats.csv'))
writetable(Uhist_table,strcat(filename,'_Uhist.csv'))
writetable(Vhist_table,strcat(filename,'_Vhist.csv'))
writetable(Bhist_table,strcat(filename,'_Bhist.csv'))
% writetable(Uspec_table,strcat(filename,'_Uspec.csv'))
% writetable(Vspec_table,strcat(filename,'_Vspec.csv'))
% writetable(Bspec_table,strcat(filename,'_Bspec.csv'))



%% 2d amp spectrum functin
function A = Aspec2d(mat,fedges)
    %mat is data matrix
    %f is non-dimesional frequency edges

    matsize = size(mat);

    %taper data to zero at edges
    taper = ones(matsize);
    taperlength = floor(max(matsize)/12);
    taper(taperlength+1:end-taperlength, 1:taperlength) = repmat(sin((1:taperlength)*pi/(2*taperlength)), matsize(1)-2*taperlength, 1);
    taper(taperlength+1:end-taperlength, end+1-taperlength:end) = repmat(cos((1:taperlength)*pi/(2*taperlength)), matsize(1)-2*taperlength, 1);
    taper(1:taperlength, taperlength+1:end-taperlength) = repmat(sin((1:taperlength)*pi/(2*taperlength)).', 1, matsize(2)-2*taperlength);
    taper(end+1-taperlength:end, taperlength+1:end-taperlength) = repmat(cos((1:taperlength)*pi/(2*taperlength)).', 1, matsize(2)-2*taperlength);
    cornerxdist = repmat(1:taperlength, taperlength,1);
    cornerydist = repmat((1:taperlength).', 1, taperlength);
    corner = (cornerxdist.^2 + cornerydist.^2).^.5;
    corner(corner > taperlength) = taperlength;
    corner = cos(corner*pi/(2*taperlength));
    taper(end+1-taperlength:end, end+1-taperlength:end) = corner;
    taper(end+1-taperlength:end, 1:taperlength) = fliplr(corner);
    taper(1:taperlength, end+1-taperlength:end) = flipud(corner);
    taper(1:taperlength, 1:taperlength) = rot90(corner,2);
    mat = mat.*taper;

    %2D fourier transform
    Fmat = fftshift(fft2(mat));
    fx = [-(matsize(2)/2:-1:0), 1:matsize(2)/2-1]/(matsize(2)); %x-frequency
    fxg = repmat(fx, matsize(1), 1);
    %lambdax = 1./fx; %x-wavelength
    %lambdaxg = 1./fxg;
    fy = [-(matsize(1)/2:-1:0), 1:matsize(1)/2-1].'/(matsize(1)); %y-frequency
    fyg = repmat(fy, 1, matsize(2));
    %lambday = 1./fy; %y-wavelength
    %lambdayg = 1./fyg;
    fg = (fxg.^2 + fyg.^2).^.5; %frequency grid
    %lambdag = 1./fg; %wavelength grid
    
    %bin 2d fft amplitudes to get 1d spectrum
    A = NaN(1, length(fedges)-1);
%     N = NaN(1, length(fedges)-1);
    for ii = 1:length(fedges)-1
        Fmat_bin = Fmat(fg >= fedges(ii) & fg < fedges(ii + 1));
        A(ii) = sum(abs(Fmat_bin));
%         N(ii) = length(Fmat_bin);
    end
    
%     figure(1),clf
%     subplot(2,1,1)
%     loglog(1./fedges(1:end-1),A)
%     subplot(2,1,2)
%     semilogx(1./fedges(1:end-1),N)
%     xlabel('wavelength')
    
end


