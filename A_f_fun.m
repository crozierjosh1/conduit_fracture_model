function [w_f, A_f, dA_f_dz] = A_f_fun(pstruct, z, A, dA_dz)
%calculates fracture width and derivative
%Josh Crozier 2019


N_f = pstruct.N_f;
d_fL = pstruct.d_fL;
d_fU = pstruct.d_fU;
w_fmax = pstruct.w_fmax;

r_c = sqrt(A/pi);
dr_c_dz = dA_dz/(2*sqrt(A*pi));

%fracture width as function of depth
if z <= -d_fL
    w_f = 0;
    dw_f_dz = 0;
elseif z > -d_fL && z < -d_fU
    %transition
    t = (z + d_fL)/(-d_fU + d_fL);
    w_f = 0 + 1/2*(0 - w_fmax)*(cos(pi*t) - 1);
    dw_f_dz = pi*w_fmax*sin(pi*(d_fL + z)/(d_fL - d_fU))/(2*(d_fL - d_fU));
    
%     %t = (z-zf)/(df*ztaperfrac);
%     %wf = (1E-18)^(1-t)*wf^t;
%     logistic_k = 1/(d_fL-d_fU)*40;
%     logistic_z0 = z_fL + (d_fL-d_fU)/2;
%     w_f = pstruct.w_fmax/(1+exp(-logistic_k*(z-logistic_z0)));
%     dw_f_dz = (logistic_k*pstruct.w_fmax*exp(-logistic_k*(z - logistic_z0)))...
%         /(exp(-logistic_k*(z - logistic_z0)) + 1)^2;

else
    w_f = w_fmax;
    dw_f_dz = 0;
end

% calculate fracture cross-sectional area

A_f = w_f*N_f*sqrt(2)*r_c;
dA_f_dz = dw_f_dz*N_f*sqrt(2)*r_c + w_f*N_f*sqrt(2)*dr_c_dz;
end