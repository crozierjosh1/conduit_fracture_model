function res = bcfun_effectiveK(var_vec_0, var_vec_end, param_vec, pstruct)
%%boundary conditions for bvp solvers
%josh crozier 2019

Patm = pstruct.Patm;
target_P0 = pstruct.P0;
gasConst = pstruct.gasConst;
Temp = pstruct.Temp;
rho_m = pstruct.rho_m;
n_v0 = pstruct.n_v0;

P0 = var_vec_0(1);
Pend = var_vec_end(1);
% phiend = var_vec_end(2);
phi0 = var_vec_0(2);

%base pressure
res_P0 = P0 - target_P0;

%atmpospheric pressure
res_Pend = Pend - Patm;

%porosity in no-flux case
rho_g0 = P0/(gasConst*Temp);
[n_d0, ~] = solubility_fun(pstruct, P0);
n_g0 = (n_v0 - n_d0)/(1 - n_d0);
if n_g0 < 0
    n_g0 = 0;
end
%assuming no fractures initially, and no gas flux
%!!!not properly accounting for crystals!!!!
target_phi0 = (n_g0/rho_g0)...
    /(n_g0/rho_g0 + (1 - n_g0)/rho_m);
res_phi0 = phi0 - target_phi0;


%total
res = [res_P0; res_Pend; res_phi0];

end

