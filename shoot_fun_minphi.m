function [odeSolution,pstruct,sol_converged,shootvar_val] = shoot_fun_minphi(pstruct,shoot,disptext)
% Shooting Method
%Josh Crozier 2020
%!!!not completely general, currently calculates error based on ode
%termination x value only!!!

shoot.error_vec = [-Inf,Inf,Inf];
shoot.val_vec = [shoot.lower_bound,NaN,shoot.upper_bound];
shoot.secant_count = 0;
sol_converged = false;

%% main loop
for shoot_iter = 1:shoot.shoot_max_iter
    if shoot_iter == 1
        %initial lower bound
        shoot_index = 1;
    elseif shoot_iter == 2
        %initial upper bound
        shoot_index = 3;
    else
        shoot_index = 2;
        
        if isnan(shoot.error_vec(1)) || isnan(shoot.error_vec(3))
            if abs(shoot.val_vec(3) - shoot.val_vec(1)) < shoot.min_step
                %shoot step smaller than min step
                disp(strcat(disptext, 'shoot step smaller than min step (NaN bounded case)',...
                    ' iter:', num2str(shoot_iter)))
                break
            end
        end
        
        if mod(shoot_iter,5) == 1
            %print progress
            disp(strcat(disptext, ' iter:', num2str(shoot_iter)))
        end
        
        
        %% secant method
        if (~isnan(shoot.error_vec(1)) && ~isnan(shoot.error_vec(3))) ...
                && (mod(shoot.secant_count,shoot.secant_inc) == 0) && ...
            ~(isinf(shoot.error_vec(1)) || isinf(shoot.error_vec(3)))
            if abs(log10(shoot.val_vec(3)/shoot.val_vec(1))) < 1
                %if ~multiple order-magnitude differnece
                shoot.slope = (shoot.error_vec(3) - shoot.error_vec(1))...
                    /(shoot.val_vec(3) - shoot.val_vec(1));
                shoot.val_vec(shoot_index) = shoot.val_vec(1) - shoot.error_vec(1)/shoot.slope;
            else

                shoot.slope = (shoot.error_vec(3) - shoot.error_vec(1))...
                    /(log10(shoot.val_vec(3)) - log10(shoot.val_vec(1)));
                shoot.val_vec(shoot_index) = 10^(log10(shoot.val_vec(1)) ...
                    - shoot.error_vec(1)/shoot.slope);
            end
         
        
        %% bisection method
        else
            if abs(log10(shoot.val_vec(3)/shoot.val_vec(1))) < 1
                %if ~multiple order-magnitude differnece
                shoot.val_vec(shoot_index) = (shoot.val_vec(1) + shoot.val_vec(3))/2;
            else
                shoot.val_vec(shoot_index) = 10^((log10(shoot.val_vec(1)) ...
                    + log10(shoot.val_vec(3)))/2);
            end
        end
        
        if (~isnan(shoot.error_vec(1)) && ~isnan(shoot.error_vec(3)))
            shoot.secant_count = shoot.secant_count + 1; %increment count
        else
            shoot.secant_count = 0; %reset anytime hit NaN
        end
        
    end %end set shoot index and/or take step

    %% solve ode
    %initial conditions
    shootvar_val = shoot.val_vec(shoot_index);
    [pstruct,init_var_vec] = shoot.shoot_IC_fun(pstruct,shoot,shootvar_val);

    %!!!need to set event function here to use updated pstruct value!!!
    shoot.ode_options.Events = @(z,var_vec) ode_event_fun_sepP(z,var_vec,pstruct);

%     if shoot.plot_ode
%         figure() %new figure to plot ode progress in
%     end
    
    if strcmp(shoot.ode_solver,'23s')
        warning('off','MATLAB:ode23s:IntegrationTolNotMet')
        odeSolution = ode23s(@(z,var_vec) odeEquations_minphi(z,var_vec,[],pstruct),...
            shoot.zspan, init_var_vec, shoot.ode_options);
        warning('on','MATLAB:ode23s:IntegrationTolNotMet')
        
    elseif strcmp(shoot.ode_solver,'23t')
        warning('off','MATLAB:ode23t:IntegrationTolNotMet')
        odeSolution = ode23t(@(z,var_vec) odeEquations_minphi(z,var_vec,[],pstruct),...
            shoot.zspan, init_var_vec, shoot.ode_options);
        warning('on','MATLAB:ode23t:IntegrationTolNotMet')
        
    elseif strcmp(shoot.ode_solver,'23tb')
        warning('off','MATLAB:ode23s:IntegrationTolNotMet')
        odeSolution = ode23tb(@(z,var_vec) odeEquations_minphi(z,var_vec,[],pstruct),...
            shoot.zspan, init_var_vec, shoot.ode_options);
        warning('on','MATLAB:ode23tb:IntegrationTolNotMet')
        
    elseif strcmp(shoot.ode_solver,'15s')
        warning('off','MATLAB:ode15s:IntegrationTolNotMet')
        odeSolution = ode15s(@(z,var_vec) odeEquations_minphi(z,var_vec,[],pstruct),...
            shoot.zspan, init_var_vec, shoot.ode_options);
        warning('on','MATLAB:ode15s:IntegrationTolNotMet')
        
    elseif strcmp(shoot.ode_solver,'113')
        warning('off','MATLAB:ode113:IntegrationTolNotMet')
        odeSolution = ode113(@(z,var_vec) odeEquations_minphi(z,var_vec,[],pstruct),...
            shoot.zspan, init_var_vec, shoot.ode_options);
        warning('on','MATLAB:ode113:IntegrationTolNotMet')
        
    elseif strcmp(shoot.ode_solver,'23')
        warning('off','MATLAB:ode23:IntegrationTolNotMet')
        odeSolution = ode23(@(z,var_vec) odeEquations_minphi(z,var_vec,[],pstruct),...
            shoot.zspan, init_var_vec, shoot.ode_options);
        warning('on','MATLAB:ode23:IntegrationTolNotMet')
        
    elseif strcmp(shoot.ode_solver,'45')
        warning('off','MATLAB:ode45:IntegrationTolNotMet')
        odeSolution = ode45(@(z,var_vec) odeEquations_minphi(z,var_vec,[],pstruct),...
            shoot.zspan, init_var_vec, shoot.ode_options);
        warning('on','MATLAB:ode45:IntegrationTolNotMet')
        
    else
        error('invalid ode solver')
    end
    
    %% calculate error  
    %!!!not completely general, currently calculates error based on ode
    %termination x value only!!!
    if odeSolution.ie == 4
        %atmospheric pressure condition
        shoot.error = odeSolution.x(end) - shoot.target_xend;
    elseif isempty(odeSolution.ie) && odeSolution.y(1,end) < 10*pstruct.Patm
        %!!!!!!hardcoded in!!!!!!!!
        %seems to be needed in some cases where pressure crashes to zero
        %quickly enough that steps need to be smaller than machine precision
        shoot.error = odeSolution.x(end) - shoot.target_xend;
    elseif isempty(odeSolution.ie)
        %solution ran to end of domain
        shoot.error = Inf;
    else
        shoot.error = NaN;
    end
    shoot.error_vec(shoot_index) = shoot.error;

    
    %% plot progress
    if shoot.shoot_plot
        if shoot_iter > 2
            if shoot_iter == 3
                shoot.fig = figure();
                clf
                hold on
            elseif shoot_iter > 3
                figure(shoot.fig)
                delete([shoot.p1,shoot.p2,shoot.p3])
            end
            if shoot_iter >= 3
                shoot.error_vec_plot = shoot.error_vec;
                shoot.error_vec_plot(isnan(shoot.error_vec)) = 0;
                shoot.error_vec_plot(isinf(shoot.error_vec)) = 0;
                if isnan(shoot.error_vec(1))
                    plot(shoot.val_vec(1),shoot.error_vec_plot(1),'kx')
                    shoot.p1 = plot(shoot.val_vec(1),shoot.error_vec_plot(1),'rx');
                elseif isinf(shoot.error_vec(1))
                    plot(shoot.val_vec(1),shoot.error_vec_plot(1),'ks')
                    shoot.p1 = plot(shoot.val_vec(1),shoot.error_vec_plot(1),'rs');
                else
                    plot(shoot.val_vec(1),shoot.error_vec_plot(1),'ko')
                    shoot.p1 = plot(shoot.val_vec(1),shoot.error_vec_plot(1),'ro');
                end
                if shoot_iter == 3
                    set(gca,'xscale','Log')
                    xlabel('Value')
                    ylabel('Error (NaN shown as x, Inf as square)')
                    grid on
                end
                if isnan(shoot.error_vec(2))
                    plot(shoot.val_vec(2),shoot.error_vec_plot(2),'kx')
                    shoot.p2 = plot(shoot.val_vec(2),shoot.error_vec_plot(2),'gx');
                elseif isinf(shoot.error_vec(2))
                    plot(shoot.val_vec(2),shoot.error_vec_plot(2),'ks')
                    shoot.p2 = plot(shoot.val_vec(2),shoot.error_vec_plot(2),'gs');
                else
                    plot(shoot.val_vec(2),shoot.error_vec_plot(2),'ko')
                    shoot.p2 = plot(shoot.val_vec(2),shoot.error_vec_plot(2),'go');
                end
                if isnan(shoot.error_vec(3))
                    plot(shoot.val_vec(3),shoot.error_vec_plot(3),'kx')
                    shoot.p3 = plot(shoot.val_vec(3),shoot.error_vec_plot(3),'bx');
                elseif isinf(shoot.error_vec(3))
                    plot(shoot.val_vec(3),shoot.error_vec_plot(3),'ks')
                    shoot.p3 = plot(shoot.val_vec(3),shoot.error_vec_plot(3),'bs');
                else
                    plot(shoot.val_vec(3),shoot.error_vec_plot(3),'ko')
                    shoot.p3 = plot(shoot.val_vec(3),shoot.error_vec_plot(3),'bo');
                end
    %             text(shoot.val_vec(1),shoot.error_vec_plot(1),num2str(iter),'Color','r');
    %             hold on
    %             text(shoot.val_vec(2),shoot.error_vec_plot(2),num2str(iter),'Color','g');
    %             text(shoot.val_vec(3),shoot.error_vec_plot(3),num2str(iter),'Color','b');
            end
        end
    end %end plot

    
    %% decide what to do next
    if ~isnan(shoot.error) && abs(shoot.error) < shoot.shoot_tol
        %suitable solution found
        sol_converged = true;
        break

    elseif shoot_index == 2

%             if sign(shoot.error_vec(1)) == sign(shoot.error_vec(3)) ||...
%                     (isnan(shoot.error_vec(1)) && isnan(shoot.error_vec(3))) || ...
%                     (isnan(shoot.error_vec(2)) && sum(isnan(shoot.error_vec([1,3]))) == 0)
%                 %sign of edges same or if NaN value only in middle
%                 %may stop if encounter local extrema or NaN in between good values
        if sum(sign(shoot.error_vec)) == -3 
            %only stop if all signs equal
            %so for samples like [- + -] will use first interval, which
            %may not be where actual solution or only solution is
            disp(strcat(disptext,'Shooting cant continue all less than target',...
                    ' iter:', num2str(shoot_iter)))
            break
            
        elseif sum(sign(shoot.error_vec)) == 3 
            %only stop if all signs equal
            %so for samples like [- + -] will use first interval, which
            %may not be where actual solution or only solution is
            disp(strcat(disptext, 'Shooting cant continue all greater than target',...
                    ' iter:', num2str(shoot_iter)))
            break
            
        elseif sum(isnan(shoot.error_vec)) == 3
            %stop if all NaN
            disp(strcat(disptext, 'Shooting cant continue all NaN',...
                    ' iter:', num2str(shoot_iter)))
            break

        else
            %create vector with +- 2 for pos-neg diff or +- 1 for pos-nan diff
            shoot.diff = diff(sign(shoot.error_vec));
            shoot.zerosign = sign(shoot.error_vec);
            shoot.zerosign(isnan(shoot.zerosign)) = 0;
            shoot.diff(isnan(shoot.diff)) = 0;
            shoot.diff = shoot.diff + diff(shoot.zerosign);
            if shoot.diff(1) < 0
                shoot.error_vec = [shoot.error_vec(2),NaN,shoot.error_vec(1)];
                shoot.val_vec = [shoot.val_vec(2),NaN,shoot.val_vec(1)];
            elseif shoot.diff(1) > 0
                shoot.error_vec = [shoot.error_vec(1),NaN,shoot.error_vec(2)];
                shoot.val_vec = [shoot.val_vec(1),NaN,shoot.val_vec(2)];
            elseif shoot.diff(2) < 0
                shoot.error_vec = [shoot.error_vec(3),NaN,shoot.error_vec(2)];
                shoot.val_vec = [shoot.val_vec(3),NaN,shoot.val_vec(2)];
            elseif shoot.diff(2) > 0
                shoot.error_vec = [shoot.error_vec(2),NaN,shoot.error_vec(3)];
                shoot.val_vec = [shoot.val_vec(2),NaN,shoot.val_vec(3)];    
            else
                %just in case
                error('!!!Shooting Conditions not coded right!!!')
            end
        end
    end
end %end shoot iteration

end %end function

